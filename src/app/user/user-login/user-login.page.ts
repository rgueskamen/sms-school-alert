import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorAuthService } from 'src/app/shared/service/error-auth.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { UserService } from '../service/user-service.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.page.html',
  styleUrls: ['./user-login.page.scss'],
})
export class UserLoginPage implements OnInit {

  login: FormGroup;
  validationMessages: any;
  canShow: boolean;
  loading: boolean;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private nav: NavController,
    private translate: TranslateService,
    private ui: UiService,
    private userservice: UserService,
    private error: ErrorSystemService,
    private userError: ErrorAuthService

  ) {
    this.canShow = false;
    this.loading = false;
  }


  ngOnInit() {
    this.initFormMessage();
    this.initLoginForm();
  }

  // Form getters
  get matricule() {
    return this.login.get('matricule');
  }

  get password() {
    return this.login.get('password');
  }

  // Init the register form message  
  initFormMessage() {


    this.translate.get([
      'FORM_FIELD_MATRICULE_ERROR_TEXT','FORM_FIELD_PASSWORD_ERROR_TEXT'
    ]).subscribe(value => {

      this.validationMessages = {
        matricule: [
          { type: 'required', message: value.FORM_FIELD_MATRICULE_ERROR_TEXT }
        ],
        password: [
          { type: 'required', message: value.FORM_FIELD_PASSWORD_ERROR_TEXT }
        ]
      };
  });
  }

  // Init the login form
  initLoginForm() {
    this.login = this.fb.group({
      matricule: this.fb.control('', [
        Validators.required
      ]),
      password: this.fb.control('', [
        Validators.required
      ])
    });
  }

  // can show passord
  updateType() {
    this.canShow = !this.canShow;
  }


  // login the account
  onLogin() {

     this.translate.get('LOGIN_LOADING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });
    this.loading = true;
    this.userservice.loginUser(this.login.value).subscribe(reponse => {

      this.ui.dismissLoading();
      this.loading = false;
      if (reponse && reponse.message === "success") {

        // set the session data
        this.userservice.setUserToken(reponse.token);
        this.userservice.setUserData(reponse.user);
        this.userservice.setUserSubscription(reponse.souscription);
        this.userservice.setUserSecret(this.login.value);
        this.initLoginForm();
        // check if it's user first login
        if (reponse.first_connexion) {
          // redirect to update password
          this.userservice.setUserFistLogin();
          this.nav.setDirection('root');
          this.router.navigate(['user/user-change-pass', this.login.value.password]);
        } else {
          // check role and redirect
          this.userservice.sessionRedirection();
        }
      }

    }, error => {

      this.ui.dismissLoading();
      this.loading = false;

      if (error && error.error && error.error.message === "error") {
        this.userError.manageUserError(error);
      } else {
        this.error.manageSystemError(error);
      }

    }); 

  }
 

}
