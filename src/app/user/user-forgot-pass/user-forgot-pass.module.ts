import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserForgotPassPageRoutingModule } from './user-forgot-pass-routing.module';

import { UserForgotPassPage } from './user-forgot-pass.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    UserForgotPassPageRoutingModule
  ],
  declarations: [UserForgotPassPage]
})
export class UserForgotPassPageModule {}
