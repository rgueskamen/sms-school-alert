import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorAuthService } from 'src/app/shared/service/error-auth.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { UserService } from '../service/user-service.service';

@Component({
  selector: 'app-user-forgot-pass',
  templateUrl: './user-forgot-pass.page.html',
  styleUrls: ['./user-forgot-pass.page.scss'],
})
export class UserForgotPassPage implements OnInit {

  forgot: FormGroup;
  isMatriculeValid: boolean;
  loading: boolean;
  canShow: boolean;

  constructor(
    private fb: FormBuilder,
    private ui: UiService,
    private userservice: UserService,
    private translate: TranslateService,
    private userError: ErrorAuthService,
    private error: ErrorSystemService,
    private router: Router,
    private nav: NavController
  ) {
    this.isMatriculeValid = false;
  }

  ngOnInit() {
    this.initForgotForm();
  }


  // Form getters
  get matricule () {
    return this.forgot.get('matricule');
  }

  get code () {
    return this.forgot.get('code');
  }

  get password () {
    return this.forgot.get('password');
  }

    // init the form
    initForgotForm() {
      this.forgot = this.fb.group({
        matricule : this.fb.control('', [ Validators.required]),
       code: this.fb.control('', [Validators.required]),
       password: this.fb.control('', [Validators.required])
      });
    }

    // can show passord
    updateType() {
      this.canShow = !this.canShow;
    }


    // get the code
    getCode() {

      this.translate.get('CODE_LOADING_TEXT').subscribe(trans => {
        this.ui.presentLoading(trans);
      });
      this.loading = true;
      const param = { matricule : this.forgot.value.matricule}
      this.userservice.getPasswordCode(param).subscribe(reponse => {

        this.ui.dismissLoading();
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.isMatriculeValid = true;
          this.forgot.get('code').setValue(reponse.code);
        }

      }, error => {

        this.ui.dismissLoading();
        this.loading = false;

        if (error && error.error && error.error.message === 'error') {
          if (error && error.error && error.error.matricule_not_exist) {
            this.translate.get('MATRICULE_NOT_EXIST_TEXT',{matricule:this.forgot.value.matricule}).subscribe(value => {
              this.ui.presentToast(value);
            });
            this.forgot.get('matricule').setValue('');
          }
        } else {
          this.error.manageSystemError(error);
        }

      });

    }

  // login the account
  onLogin(newCredentials: any) {
    this.userservice.loginUser(newCredentials).subscribe(reponse => {

      if (reponse && reponse.message === 'success') {
        // set the session data
        this.userservice.setUserToken(reponse.token);
        this.userservice.setUserData(reponse.user);
        this.userservice.setUserSubscription(reponse.souscription);
        this.userservice.setUserSecret(newCredentials);
        this.initForgotForm();
        this.nav.setDirection('root');
        this.router.navigate(['user/user-profil']);
      }
      this.ui.dismissLoading();
      this.loading = false;

    }, error => {

      this.ui.dismissLoading();
      this.loading = false;

      if (error && error.error && error.error.message === 'error') {
        this.userError.manageUserError(error);
      } else {
        this.error.manageSystemError(error);
      }

    });

  }

  // reset the password
  resetPassword(data: any) {
    this.userservice.updateForgotPassword(data).subscribe(reponse => {

      if (reponse && reponse.message === 'success') {
        const credentials = this.userservice.getUserSecret();
        const  newCredentials = {
          matricule: credentials.matricule,
          password: this.forgot.value.password
        }
        this.onLogin(newCredentials);
      } else {
        this.ui.dismissLoading();
        this.loading = false;
      }

    }, error => {

      this.ui.dismissLoading();
      this.loading = false;

      if (error && error.error && error.error.message === 'error') {

        this.userError.manageUserError(error);
      } else {
        this.error.manageSystemError(error);
      }

    });

  }


  // send data
  reset() {

   this.translate.get('RESET_LOADING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });
    this.loading = true;
    this.userservice.confirmCode(this.forgot.value).subscribe(reponse => {

      if (reponse && reponse.message === 'success') {
        const  param = {
          user_id: reponse.user.id,
          password: this.forgot.value.password
        };
        this.resetPassword(param);
      } else {
        this.ui.dismissLoading();
        this.loading = false;
      }

    }, error => {

      this.ui.dismissLoading();
      this.loading = false;

      if (error && error.error && error.error.message === 'error') {
          this.userError.manageUserError(error);
      } else {
        this.error.manageSystemError(error);
      }
    });
  }
}
