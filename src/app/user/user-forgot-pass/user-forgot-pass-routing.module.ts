import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserForgotPassPage } from './user-forgot-pass.page';

const routes: Routes = [
  {
    path: '',
    component: UserForgotPassPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserForgotPassPageRoutingModule {}
