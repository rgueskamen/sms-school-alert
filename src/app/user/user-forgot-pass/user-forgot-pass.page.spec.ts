import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserForgotPassPage } from './user-forgot-pass.page';

describe('UserForgotPassPage', () => {
  let component: UserForgotPassPage;
  let fixture: ComponentFixture<UserForgotPassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserForgotPassPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserForgotPassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
