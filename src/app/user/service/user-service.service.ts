import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/shared/service/api.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private api: ApiService,
    private route: Router,
    private nav: NavController
  ) { }


  // set the user first login
  setUserFistLogin() {
    localStorage.setItem('sms-user-first-login', 'yes');
  }

  // get the user first login
  getUserFisrtLogin() {
    const hasLogin = localStorage.getItem('sms-user-first-login');
    return hasLogin ? hasLogin : null;
  }

  // remove the user first login
  removeUserFistLogin() {
    localStorage.removeItem('sms-user-first-login');
  }

  // set the user token
  setUserToken(token: any) {
    localStorage.setItem('sms-user-token', token);
  }

  // get the user token
  getToken() {
    const token = localStorage.getItem('sms-user-token');
    return token ? token : null;
  }

  // set the user secret
  setUserSecret(credentials) {
    localStorage.setItem('sms-user-credentials', btoa(JSON.stringify(credentials)));
  }

  // Get the user secret
  getUserSecret() {
    const credentials = localStorage.getItem('sms-user-credentials');
    return credentials ? JSON.parse(atob(credentials)) : null;
  }

  // set the user data
  setUserData(data: any) {
    localStorage.setItem('sms-user-data', btoa(JSON.stringify(data)));
  }

  // get the user data
  getUserData() {
    const data = localStorage.getItem('sms-user-data');
    return data ? JSON.parse(atob(data)) : null;
  }

  // set the user subscription
  setUserSubscription(data: any) {
    localStorage.setItem('sms-user-subscription', JSON.stringify(data));
  }

  // get the user subscription
  getUserSubscription() {
    const data = localStorage.getItem('sms-user-subscription');
    return data ? JSON.parse(data) : null;
  }


  // set the user role
  setUserRole(data: any) {
    localStorage.setItem('sms-user-role', JSON.stringify(data));
  }

  // get the user role
  getUserRole() {
    const data = localStorage.getItem('sms-user-role');
    return data ? JSON.parse(data) : null;
  }

  // set the user country
  setUserCountry(data: any) {
    localStorage.setItem('sms-user-country',JSON.stringify(data));
  }

  // get the user country
  getUserCountry() {
    const data = localStorage.getItem('sms-user-country');
    return data ? JSON.parse(data) : null;
  }

  // set the user school
  setUserSchool(data: any) {
    localStorage.setItem('sms-user-school', JSON.stringify(data));
  }

  // get the user school
  getUserSchool() {
    const data = localStorage.getItem('sms-user-school');
    return data ? JSON.parse(data) : null;
  }

  // set the user school
  setUserSchoolYear(data: any) {
    localStorage.setItem('sms-user-school-year', JSON.stringify(data));
  }

  // get the user school
  getUserSchoolYear() {
    const data = localStorage.getItem('sms-user-school-year');
    return data ? JSON.parse(data) : null;
  }

  // log out the user
  logout() {
    localStorage.removeItem('sms-user-credentials');
    localStorage.removeItem('sms-user-first-login');
    localStorage.removeItem('sms-user-token');
    localStorage.removeItem('sms-user-data');
    localStorage.removeItem('sms-user-subscription');
    localStorage.removeItem('sms-user-role');
    localStorage.removeItem('sms-user-country');
    localStorage.removeItem('sms-user-school');
    localStorage.removeItem('sms-user-school-year');
  }

  // login the user
  loginUser(data: any) {
    return this.api.post('usermanagement/login/user', data);
  }

  // get the user profil
  getUserProfil(token: string) {
    return this.api.get(`usermanagement/get/profil/user/${token}`);
  }

  // update the user profil
  updateUserProfile(data: any, token: string) {
    return this.api.post(`usermanagement/edit/profil/user/${token}`, data);
  }

  // Update password
  updatePassword(data: any, token: string) {
    return this.api.post(`usermanagement/change/password/${token}`, data);
  }

  // Get the code to update password
  getPasswordCode(data: any) {
    return this.api.post(`passwordreset/send/code/toChangePassword`, data);
  }

  // confirm the user code
  confirmCode(data: any) {
    return this.api.post(`passwordreset/confirm/code`, data);
  }

  // Update the forgot password
  updateForgotPassword(data: any) {
    return this.api.post(`passwordreset/change/password`, data);
  }

  // session redirection
  sessionRedirection() {
    const userData = this.getUserData();
    let url = '';
    switch (parseInt(userData.role_id)) {
        case 8:
            url = '/student';
            break;
        case 9:
            url = '/teacher';
            break;
        case 10:
            url = '/parent';   
            break;
        default:
            url = '/tutorial';
            break; 

    }
    this.nav.setDirection('root');
    this.route.navigateByUrl(url);
  }

}
