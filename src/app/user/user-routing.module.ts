import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'user-login',
    loadChildren: () => import('./user-login/user-login.module').then( m => m.UserLoginPageModule)
  },
  {
    path: 'user-registration',
    loadChildren: () => import('./user-registration/user-registration.module').then( m => m.UserRegistrationPageModule)
  },
  {
    path: 'user-profil',
    loadChildren: () => import('./user-profil/user-profil.module').then( m => m.UserProfilPageModule)
  },
  {
    path: 'user-forgot-pass',
    loadChildren: () => import('./user-forgot-pass/user-forgot-pass.module').then( m => m.UserForgotPassPageModule)
  },
  {
    path: 'user-change-pass/:password',
    loadChildren: () => import('./user-change-pass/user-change-pass.module').then( m => m.UserChangePassPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
