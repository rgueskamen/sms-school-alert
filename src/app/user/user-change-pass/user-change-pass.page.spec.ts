import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UserChangePassPage } from './user-change-pass.page';

describe('UserForgotPassPage', () => {
  let component: UserChangePassPage;
  let fixture: ComponentFixture<UserChangePassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserChangePassPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UserChangePassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
