import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorAuthService } from 'src/app/shared/service/error-auth.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { UserService } from '../service/user-service.service';

@Component({
  selector: 'app-user-change-pass',
  templateUrl: './user-change-pass.page.html',
  styleUrls: ['./user-change-pass.page.scss'],
})
export class UserChangePassPage implements OnInit {

  forgot: FormGroup;
  validationMessages: any;
  loading: boolean;
  oldpassword: string;
  canShow: boolean;

  constructor(
    private fb: FormBuilder,
    private translate: TranslateService,
    private ui: UiService,
    private userError: ErrorAuthService,
    private error: ErrorSystemService,
    private userservice: UserService,
    private activeRoute: ActivatedRoute,
    private nav: NavController,
    private router: Router
  ) {
      this.loading = false;
      this.canShow = false;
      this.oldpassword = this.activeRoute.snapshot.params.password;
  }


  ngOnInit() {
    this.initFormMessage();
    this.initForgotForm();
  }


  // Forn getters
  get passwordNew() {
    return this.forgot.get('ancien_password');
  }

  get passwordOld() {
    return this.forgot.get('nouveau_password');
  }

  // Init the register form message 
  initFormMessage() {
    this.translate.get([
      'FORM_FIELD_REQUIRED_ERROR_TEXT',
    ]).subscribe(value => {

      this.validationMessages = {
        passwordNew: [
          { type: 'required', message: value.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        passwordOld: [
          { type: 'required', message: value.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }


  // init the form
  initForgotForm() {
    this.forgot = this.fb.group({
      ancien_password: this.fb.control(this.oldpassword ? this.oldpassword : '', [Validators.required]),
      nouveau_password: this.fb.control('', [Validators.required])
    });
  }


    // can show passord
    updateType() {
      this.canShow = !this.canShow;
    }
  

  // skip the step
  ignoreStep() {
    this.nav.setDirection('root');
    this.router.navigate(['user/user-profil']);
  }

  // login the account
  onLogin(newCredentials: any) {
    this.userservice.loginUser(newCredentials).subscribe(reponse => {

      if (reponse && reponse.message === "success") {
        // set the session data
        this.userservice.setUserToken(reponse.token);
        this.userservice.setUserData(reponse.user);
        this.userservice.setUserSubscription(reponse.souscription);
        this.userservice.setUserSecret(newCredentials);
        this.initForgotForm();
        this.nav.setDirection('root');
        this.router.navigate(['user/user-profil']);
      }

      this.ui.dismissLoading();
      this.loading = false;

    }, error => {

      this.ui.dismissLoading();
      this.loading = false;

      if (error && error.error && error.error.message === "error") {
        this.userError.manageUserError(error);
      } else {
        this.error.manageSystemError(error);
      }

    });

  }


  // Reset the password
  reset() {

    this.translate.get('RESET_LOADING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });
    this.loading = true;
    const token = this.userservice.getToken();
    this.userservice.updatePassword(this.forgot.value,token).subscribe(reponse => {

      if (reponse && reponse.message === "success") {
        const credentials = this.userservice.getUserSecret();
        const  newCredentials = {
          matricule: credentials.matricule,
          password: this.forgot.value.nouveau_password
        }
        this.onLogin(newCredentials);
      } else {
        this.ui.dismissLoading();
        this.loading = false;
      }

    }, error => {
      this.loading = false;
      if (error && error.error && error.error.message === "error") {

        if (error && error.error && error.error.user_not_found) {
            this.loading = true;
            this.error.renewSession().then((data: any) => {
                this.ui.dismissLoading();
                if (data && data.result === "OK") {
                    this.reset();
                } else {
                  this.loading = false;
                }
            });
        } else {
          this.ui.dismissLoading();
          this.userError.manageUserError(error);
        }
      } else {
        this.ui.dismissLoading();
        this.error.manageSystemError(error);
      }

    }); 

  }

}
