import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ErrorAuthService } from 'src/app/shared/service/error-auth.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { FormsService } from 'src/app/shared/service/forms.service';
import { UserService } from '../service/user-service.service';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.page.html',
  styleUrls: ['./user-registration.page.scss'],
})
export class UserRegistrationPage implements OnInit {
  register: FormGroup;
  validationMessages: any;
  roles: any;
  countries: any;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private formService: FormsService,
    private error: ErrorSystemService,
    private userError: ErrorAuthService,
    private translate: TranslateService
  ) {
      this.roles = [];
      this.countries = [];
   }

  onRegister() {
    if (this.register.valid) {
      console.log(this.register.value);
    }
  }

  ngOnInit() {
      this.getRoles();
      this.getCountries();
      this.initUserForm();
  }

  // Init the register form message 
  initFormMessage() {

    this.translate.get([
      'REGISTER_COUNTRY_TEXT',
      'REGISTER_COUNTRY_PREFIX',
      'REGISTER_COUNTRY_PREFIX_LENGTH',
      'REGISTER_PHONE_REQUIRED',
      'REGISTER_PHONE_INVALID'
    ]).subscribe(value => {

      this.validationMessages = {
        country_id: [
          { type: 'required', message: value.REGISTER_COUNTRY_TEXT }
        ],
        phoneid: [
          { type: 'required', message: value.REGISTER_COUNTRY_PREFIX },
          { type: 'maxlength', message: value.REGISTER_COUNTRY_PREFIX_LENGTH }
        ],
        phone: [
          { type: 'required', message: value.REGISTER_PHONE_REQUIRED },
          { type: 'pattern', message: value.REGISTER_PHONE_INVALID }
        ]
      };
    });
  }


  // init the user form
  initUserForm() {

    this.register = this.fb.group({
      first_name: this.fb.control('', [
        Validators.required,
        Validators.maxLength(150)
      ]),
      last_name: this.fb.control('', [
        Validators.required,
        Validators.maxLength(150)
      ]),
      email: this.fb.control('', [
        Validators.required,
        Validators.email
      ]),
      password: this.fb.control('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(150)
      ]),
      password_confirm: this.fb.control('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(150)
      ])
    }, {
      validators: this.passwordConfirmMatchValidator
    });

  }


  // get the user role
  getRoles() {
      this.formService.getRoles().subscribe(roles => {
        this.roles = roles;
      });
  }

  // get the countries list 
  getCountries() {
    this.formService.getCountries().subscribe(countries => {
      this.countries = countries;
    });
  } 


  passwordConfirmMatchValidator(g: FormGroup) {
    const password = g.get('password');
    const password_confirm = g.get('password_confirm');

    if (password_confirm.hasError('required') || password_confirm.hasError('minlength')) return;

    if (password.value !== password_confirm.value) {
      password_confirm.setErrors({
        mismatch: true
      });
    } else {
      password_confirm.setErrors(null);
    }
  }

}
