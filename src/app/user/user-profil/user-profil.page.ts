import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, ModalController, NavController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { ErrorAuthService } from 'src/app/shared/service/error-auth.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { FormsService } from 'src/app/shared/service/forms.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { UserService } from '../service/user-service.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { CountriesComponent } from 'src/app/shared/components/countries/countries.component';
import { PluginService } from 'src/app/shared/service/plugin.service';

@Component({
  selector: 'app-user-profil',
  templateUrl: './user-profil.page.html',
  styleUrls: ['./user-profil.page.scss'],
})
export class UserProfilPage implements OnInit {
  currentUser: any;
  defaultImage: any;
  loading: boolean;
  states: any;
  validationMessages: any;
  userDetailForm: FormGroup;
  user: any;
  languageTranslation: any;
  errorPhone: boolean;
  errorEmail: boolean;
  errorUser: boolean;
  errorUsername: boolean;
  typeMimeError: boolean;
  userPicture: string;
  canShowBackButton: string;


  constructor(
    private fb: FormBuilder,
    private location: TranslateService,
    private alertController: AlertController,
    private platform: Platform,
    private navController: NavController,
    private translate: TranslateService,
    private modatCtrl: ModalController,
    private formUtil: FormsService,
    private event: EventsService,
    private router: Router,
    private userService: UserService,
    private errorService: ErrorSystemService,
    private userError: ErrorAuthService,
    private plugin: PluginService,
    private ui: UiService,
    private api: ApiService,
    private util: UtilsService
  ) {
    this.hardwareBackButton();
    this.states = [];
    this.errorPhone = false;
    this.errorEmail = false;
    this.errorUser = false;
    this.typeMimeError = false;
    this.userPicture = null;
    this.canShowBackButton = this.userService.getUserFisrtLogin();
    this.currentUser = this.userService.getUserData();
    this.defaultImage = '../../assets/img/bg-02.png';
    this.loading = false;
  }


  ngOnInit() {
    this.validationMessage();
    this.initUserForm();
  }

  // Form getters 
  get firstName() {
    return this.userDetailForm.get('firstname');
  }

  get lastName() {
    return this.userDetailForm.get('lastname');
  }

  get phoneError() {
    return this.userDetailForm.get('phone');
  }

  get emailError() {
    return this.userDetailForm.get('email');
  }

  get gender() {
    return this.userDetailForm.get('sexe');
  }

  get country() {
    return this.userDetailForm.get('country_id');
  }

  // init the user form with his data
  initUserForm() {
    this.user = this.userService.getUserData();
    this.userDetailForm = this.fb.group({
      firstname: [this.user && this.user.firstname ? this.user.firstname: '', Validators.required],
      lastname: [this.user && this.user.lastname ? this.user.lastname : '', Validators.required],
      matricule: [this.user && this.user.matricule ? this.user.matricule : '', Validators.required],
      matriculeForm: [{ value: this.user && this.user.matricule ? this.user.matricule : '', disabled: true }],
      sexe: [this.user && this.user.sexe ? this.user.sexe : 'M', Validators.required],
      countryName: [''],
      country_id: [this.user && this.user.country_id ? this.user.country_id : '', Validators.required],
      phone: [this.user && this.user.phone ? this.user.phone : '', Validators.required],
      email: [this.user && this.user.email ? this.user.email : '', Validators.required],
      picture: [this.user && this.user.picture ? this.user.picture : '']
    });

    if (this.user.country_id) {
      this.getCountryName(this.user.country_id);
    }
    
  }

  // Validation message
  validationMessage() {
    this.location.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        firstname: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        lastName: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        userName: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],

        phone: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        country: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],

        email: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });

  }


  // check the phone
  checkPhone(phone: string) {
    this.errorPhone = this.formUtil.validatePhone(phone);
    if (!this.errorPhone) {
      this.phoneError.setErrors({ invalid: true });
    }
  }

  // check the email
  checkEmail(email: string) {
    this.errorEmail = this.formUtil.validateEmail(email);
    if (!this.errorEmail) {
      this.emailError.setErrors({ invalid: true });
    }
  }

  // cancel the profile update
  cancel() {
    this.initUserForm();
  }

  goback() {
      // check the user role and redirect
      this.userService.sessionRedirection();
  }

  // Get the user picture
  getPicture() {
      this.plugin.getPicture().subscribe((picture: any) => {
          if (picture) {
            setTimeout(() => {
              this.userDetailForm.get('picture').setValue(picture);
              this.userPicture = picture;
            }, 300);
          } else {
            this.userPicture = this.user && this.user.picture ? `${this.user.picture}` : null;
          }
        }); 
  }

  // open the countries modal
  showCountries() {
    this.modatCtrl
      .create({
        component: CountriesComponent
      })
      .then(modalEl => {
        modalEl.present();
        modalEl.onDidDismiss().then((ans) => {
          if (ans && ans.role === 'select') {
              if (ans.data) {
                this.userDetailForm.get('countryName').setValue(ans.data.country_name);
                this.userDetailForm.get('country_id').setValue(ans.data.country_id);
              }
          }
        });
      });
  }

  // check if it user first login
  isUserFisrtLogin() {
    if (this.userService.getUserFisrtLogin() === 'yes') {
      this.userService.removeUserFistLogin()
      // redirect  appropriate role
      this.userService.sessionRedirection();
    }
  }

  // show login Message
  showMessage() {
    if (localStorage.getItem('fisrt-login') !== 'yes') {
      this.location.get('COMPLETE_PROFILE_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } else {
      this.location.get('PROFILE_UPDATED_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }
  }

  // get the user profil
  getUserProfil() {
    const token = this.userService.getToken();
    this.userService.getUserProfil(token).subscribe((user: any) => {
      this.loading = false;
      this.showMessage();
      this.userService.setUserData(user.user);
      this.userService.setUserRole(user.role);
      this.userService.setUserCountry(user.country);
      this.userService.setUserSchool(user.etablissement_scolaire);
      this.userService.setUserSchoolYear(user.annee_academique);
      this.event.publish('user-data', user.user);
      this.initUserForm();
      this.isUserFisrtLogin();

    }, error => {
      this.loading = false;
      if (error.error.user_not_found) {
        this.loading = true;
        this.errorService.renewSession().then((data: any) => {
          if (data && data.result === "OK") {
            this.getUserProfil();
          } else {
            this.loading = false;
          }
        });
      } else {
        this.errorService.manageSystemError(error);
      }
    });
  }

  // Create account and tontine
  updateAccount() {
    this.loading = true;
    const token = this.userService.getToken();
    this.userService.updateUserProfile(this.userDetailForm.value, token).subscribe(
      (reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.getUserProfil();
        }
      }, error => {
        this.loading = false;
        if (error && error.error) {
          if (error && error.error && error.error.user_not_found) {
            this.loading = true;
            this.errorService.renewSession().then((data: any) => {
              if (data && data.result === "OK") {
                this.updateAccount();
              } else {
                this.loading = false;
              }
            });
          } else {
            this.userError.manageUserError(error);
          }
        } else {
          this.errorService.manageSystemError(error);
        }
      });
  }


  // confirmation message
  async confirmationMessage(translation: string[]) {

    const alert = await this.alertController.create({
      header: `${translation[0]}`,
      message: `${translation[1]}`,
      buttons: [
        {
          text: `${translation[2]}`,
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {

          }
        }, {
          text: `${translation[3]}`,
          handler: () => {
            navigator['app'].exitApp();
          }
        }
      ]
    });

    await alert.present();
  }

  // listen to back button
  hardwareBackButton() {
    this.platform.backButton.subscribe(() => {
      const url = this.router.url;
      if (url === '/user/user-profil') {
        const messages = [];
        this.translate.get(['M_EXIT_TITLE', 'M_EXIT_MESSAGE', 'CANCEL_TEXT', 'YES_TEXT'])
          .subscribe(trans => {
            messages.push(trans.M_EXIT_TITLE);
            messages.push(trans.M_EXIT_MESSAGE);
            messages.push(trans.CANCEL_TEXT);
            messages.push(trans.YES_TEXT);
            this.confirmationMessage(messages);
          });
      }
    });
  }

    // Get  country name
    getCountryName(countryId: any) {
      this.formUtil.getCountries().subscribe((countries: any) => {
        if (countries && countries.length > 0) {
          const states = this.util.orderByKeyUp(countries, 'country_name');
          const currentCountry =  states.filter((state) =>  { return parseInt(state.country_id) === parseInt(countryId) });
          if (currentCountry) {
            this.userDetailForm.get('countryName').setValue(currentCountry[0].country_name);
          }
        }
      });
    }

}
