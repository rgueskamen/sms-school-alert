import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserProfilPageRoutingModule } from './user-profil-routing.module';

import { UserProfilPage } from './user-profil.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    UserProfilPageRoutingModule
  ],
  declarations: [UserProfilPage]
})
export class UserProfilPageModule {}
