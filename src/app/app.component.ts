import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private translate: TranslateService,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
    translate.setDefaultLang('fr');
    let browserLang = this.translate.getBrowserLang();
    browserLang =  browserLang.toLocaleLowerCase(); // 'fr'  //
    translate.use(browserLang.match('en|fr') ?  'fr' : browserLang);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
