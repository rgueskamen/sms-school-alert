import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './user/service/user-service.service';


@Injectable({
    providedIn: 'root'
})
export class AppGuard implements CanLoad {

    constructor(
        private userService: UserService,
        private route: Router
    ) {}

    canLoad( route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
            if (this.userService.getToken()) {
                if(this.userService.getUserFisrtLogin() === 'yes') {
                    this.route.navigateByUrl('/user/user-profil'); 
                } else {
                  // check the user role and make the appropriate redirection
                  this.userService.sessionRedirection();
                } 
                return false;
            } else {
                return true;
            }
    }

}
