import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorClassService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }

  // Manage class error
  manageClassError(error) {

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.etablissement_scolaire_id_not_exist) {
      this.translate.get('SCHOOL_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 
    

    if (error && error.error && error.error.type_enseignement_id_not_exist) {
      this.translate.get('TYPE_LESSON_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.nom_classe_already_exist) {
      this.translate.get('CLASS_ALREADY_EXIST_TEXT',{class : error.error.value_nom }).subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.matiere_classe_id_not_exist) {
      this.translate.get('CLASS_COURSE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

   

  }

}
