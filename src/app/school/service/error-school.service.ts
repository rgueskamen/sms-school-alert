import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorSchoolService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }


  // Manage planning error
  manageSchoolError(error) {

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.etablissement_scolaire_id_not_exist) {
      this.translate.get('SCHOOL_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.country_id_not_exist) {
      this.translate.get('COUNTRY_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      }); 
    }

    if (error && error.error && error.error.type_enseignement_id_not_exist) {
      this.translate.get('TYPE_LESSON_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

  }


}
