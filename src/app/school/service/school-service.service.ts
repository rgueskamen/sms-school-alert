import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';

@Injectable({
  providedIn: 'root'
})
export class SchoolServiceService {

  constructor(
    private api: ApiService
  ) { }

    // Get types of school
    getSchoolTypes() {
      return this.api.get('etablissement/get/all/type/etablissement');
    }
  
    // Get school information
    getSchoolData(schoolId: any, token: string) {
      return this.api.get(`etablissement/get/info/etablissement/${schoolId}/${token}`);
    }

    // Update school data
    updateSchoolData(data: any, token: string) {
      return this.api.post(`etablissement/update/${token}`, data);
    }

}
