import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class ClasseServiceService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }

  // Get the list of courses
  getCourses() {
    return this.api.get('classe/get/type/enseignement');
  }

  // Add a class
  addClasses(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/get/type/enseignement/${token}`, data);
  }

  // get classes of a school
  getSchoolClasses(schoolId: number) {
    const token = this.userService.getToken();
    return this.api.get(`classe/get/all/${schoolId}/${token}`);
  }

  // Update school classes
  updateSchoolClasses(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/update/${token}`, data);
  }


  // Delete a school classes
  deleteSchoolClasses(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/delete/${token}`, data);
  }

  // Add class course
  addClassCourse(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/add/matiere/${token}`, data);
  }

  // Get the courses of school classes
  getCoursesOfSchoolClasses(classId: number) {
    const token = this.userService.getToken();
    return this.api.get(`classe/get/matiere/classe/${classId}/${token}`);
  }

  // Update a course of a class
  updateCourseOfClass(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/update/matiere/${token}`, data);
  }

  // Delete a course of classe
  deleteCourseOfClass(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`classe/delete/matiere/${token}`, data);
  }


}
