import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorAdminSchoolService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }


  // Manage admin school error
  manageAdminSchoolError(error) {

    if (error && error.error && error.error.etablissement_scolaire_id_not_exist) {
      this.translate.get('SCHOOL_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.matricule_already_exist) {
      this.translate.get('MATRICULE_ALREADY_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.country_id_not_exist) {
      this.translate.get('COUNTRY_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      }); 
    }

       
    if (error && error.error && error.error.email_already_exist) {
      this.translate.get('EMAIL_ALREADY_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.phone_already_exist) {
      this.translate.get('PHONE_ALREADY_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

 
    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    
    if (error && error.error && error.error.matiere_id_not_exist) {
      this.translate.get('COURSE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.surveillant_general_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.censeur_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.professeur_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.utilisateur_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.parent_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    } 

    if (error && error.error && error.error.professeur_id_not_matche_with_etablissement_scolaire_id) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.this_professeur_id_is_already_affected_for_this_classe_id) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.this_matiere_id_is_already_affected_for_this_classe_id) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.professeur_classe_id_not_exist) {
      this.translate.get('').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

  }


}
