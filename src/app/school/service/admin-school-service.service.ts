import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class EtabliServiceService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }

    // Create surveillant account
    createSurveillantAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/createAccount/surveillant/general/${token}`,data);
    }
  
    // Create censor account
    createCensorAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/createAccount/censeur/${token}`, data);
    }

    // create teacher account
    createTeacherAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/createAccount/professor/${token}`, data);
    }

    // Get the school human ressources
    getSchoolHR(schoolId: any) {
      const token = this.userService.getToken();
      return this.api.get(`usermanagement/get/personnel/etablissement/${schoolId}/${token}`);
    }

    // Update surveillant data
    updateSurveillantAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/update/infos/surveillant/general/${token}`,data);
    }

    // Update censor data
    updateCensorAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/update/infos/censeur/${token}`, data);
    }

    // update teacher Infos
    updateTeacherAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/update/infos/professor/${token}`, data);
    }

    // Delete a user
    deleteUser(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/delete/user/${token}`, data);
    }

    // Create school parent Account
    createSchoolParentAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/createAccount/parent/${token}`, data);
    }

    // Get parent of a school
    getSchoolParents(schoolId: any) {
      const token = this.userService.getToken();
      return this.api.get(`usermanagement/get/all/parents/${schoolId}/${token}`);
    }

    // Update school parent Account
    updateSchoolParentAccount(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/update/infos/parent/${token}`, data);
    }

    // Add student 
    addStudent(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/save/student/with/parent/${token}`, data);
    }

    // Get student with parent
    getStudentWithParent(classId: any) {
      const token = this.userService.getToken();
      return this.api.get(`usermanagement/get/students/to/a/specific/classe/${classId}/${token}`)
    }

    // Update student account 
    updateStudent(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/update/student/with/parent/${token}`, data);
    }

    // Add a teacher class and course
    assignTeacherClassAndCourse(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/attribute/professor/to/a/class/${token}`, data);
    }

    // Get the teacher classes with course
    getTeacherClassesWithCourse(schoolId: any,classId: any) {
      const token = this.userService.getToken();
      return this.api.get(`usermanagement/get/professor/class/withMatiere/${schoolId}/${classId}/${token}`);
    }

   // update a teacher class and course
   updateTeacherClassAndCourse(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/edit/attribution/professor/to/a/class/${token}`, data);
   }

   // Add a teacher class and course
   deleteAssignTeacherClassAndCourse(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/delete/attribution/professor/to/a/class/${token}`, data);
    }

    // Add a head Teacher of a class
    addHeadTeacher(data: any) {
      const token = this.userService.getToken();
      return this.api.post(`usermanagement/attribute/principal/professor/to/a/class/${token}`, data);
    }

}
