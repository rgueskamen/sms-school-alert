import { Injectable } from '@angular/core';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UiService {
  isLoadingShow = false;  
  constructor(
    public alertController: AlertController,
    private toastController: ToastController,
    public loadingController: LoadingController,
  ) { }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['OK']
    });
  
    await alert.present();
  }


  async presentLoading(messageLoading: string) {
    this.isLoadingShow = true;
    return await this.loadingController.create({
      message: messageLoading,
    }).then(a => {
      a.present().then(() => {
      const timer = setInterval(() => {
          if (!this.isLoadingShow) {
            a.dismiss().then(() => {});
            clearTimeout(timer);
          }
        }, 2000);
      
      });
    });
  }

  async dismissLoading() {
    this.isLoadingShow = false;
    return await this.loadingController.dismiss().then(() =>{});
  }

  // Present the toast message
async presentToast(messageParam: string) {
  const toast = await this.toastController.create({
    message: messageParam,
    duration: 5000,
    position: 'top'
  });
  toast.present();
}

}
