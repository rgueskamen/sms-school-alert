import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  baseUrl = 'https://restcountries.eu/rest/v2/';

  constructor(
    private http: HttpClient
  ) { }

  // get all countries
  getAllcountriesData() {
    return new Observable((observer) => {
      this.http.get(`${this.baseUrl}all`).subscribe((countries: any) => {
        observer.next(countries);
      }, error => {
        this.http.get(`assets/json/countries.json`).subscribe((countries: any) => { observer.next(countries); });
      });
    });
  }

  orderPropertyByKeyUp(dataList: any[],property: string, key: string) {

    if (dataList && dataList.length > 0) {
      let i = 0;
      let temp: any;
      while (i < dataList.length) {
        for (let j = i + 1; j < dataList.length; j++) {
          if (dataList[i][property][key] < dataList[j][property][key]) {
            temp = dataList[j];
            dataList[j] = dataList[i];
            dataList[i] = temp;
          }
        }
        i++;
      }
    } else {
      return [];
    }
    return dataList;
  }

  orderPropertyByKeyDown(dataList: any[],property: string, key: string) {

    if (dataList && dataList.length > 0) {
      let i = 0;
      let temp: any;
      while (i < dataList.length) {
        for (let j = i + 1; j < dataList.length; j++) {
          if (dataList[i][property][key] >= dataList[j][property][key]) {
            temp = dataList[j];
            dataList[j] = dataList[i];
            dataList[i] = temp;
          }
        }
        i++;
      }
    } else {
      return [];
    }
    return dataList;
  }


  orderByKeyDown(dataList: any[], key: string) {
    if (dataList && dataList.length > 0) {
      let i = 0;
      let temp: any;
      while (i < dataList.length) {
        for (let j = i + 1; j < dataList.length; j++) {
          if (dataList[i][key] >= dataList[j][key]) {
            temp = dataList[j];
            dataList[j] = dataList[i];
            dataList[i] = temp;
          }
        }
        i++;
      }
    } else {
      return [];
    }
    return dataList;
  }

  orderByKeyUp(dataList: any[], key: string) {
    if (dataList && dataList.length > 0) {
      let i = 0;
      let temp: any;
      while (i < dataList.length) {
        for (let j = i + 1; j < dataList.length; j++) {
          if (dataList[i][key] < dataList[j][key]) {
            temp = dataList[j];
            dataList[j] = dataList[i];
            dataList[i] = temp;
          }
        }
        i++;
      }
    } else {
      return [];
    }
    return dataList;
  }



}
