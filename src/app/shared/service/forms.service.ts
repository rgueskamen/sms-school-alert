import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { ErrorSystemService } from './error-system.service';

@Injectable({
  providedIn: 'root'
})
export class FormsService {
  roles: any;
  constructor(
    private api: ApiService,
    private translate: TranslateService,
    private error: ErrorSystemService
  ) { 
      this.roles = [];
  }

  // Get the user role
  getRoleService() {
    return this.api.get('usermanagement/get/roles');
  }

  // get all countries
  getAllCountries() {
    return this.api.get('usermanagement/get/countries');
  }

  validateEmail(myEmail: string) {
    const regex = /^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}[.][a-z]{2,4}$/;
    if (!regex.test(myEmail)) {
      return false;
    } else {
      return true;
    }
  }

  validatePhone(myPhone: string) {
    const regex = /^[0-9]{6,13}$/;
    if (!regex.test(myPhone)) {
      return false;
    } else {
      return true;
    }
  }

  validatePin(myPin: string) {
    const regex = /^[0-9]{5}$/;
    if (!regex.test(myPin)) {
      return false;
    } else {
      return true;
    }
  }

  validateMisaPrix(miseAprix: string) {
    const regex = /^[0-9]+$/;
    if (!regex.test(miseAprix)) {
      return false;
    } else {
      return true;
    }
  }

    // Get a random id
    getRandomId(): string {
      let text = 'M';
      const possible = 'ABCDEFGHJKLMPQRSTUVWXYZabcdefghjklmpqrstuvwxyz';
      const chiffre = '0123456789';
      for (let i = 0; i <= 5; i++) {
          text += chiffre.charAt(Math.floor(Math.random() * chiffre.length));
      }
      return text;
    }

      // get the user role
  getRoles() {
    return new Observable(observer => {

      this.getRoleService().subscribe(reponse => {
        if (reponse && reponse.message === "success") {
          this.roles = reponse.roles;
          this.translate.get(['ROLE_TEXT_1','ROLE_TEXT_2','ROLE_TEXT_3','ROLE_TEXT_4', 
          'ROLE_TEXT_5','ROLE_TEXT_6','ROLE_TEXT_7','ROLE_TEXT_8','ROLE_TEXT_9']).subscribe(trans => {
            this.roles.forEach((role, index, self) => {
              this.roles[index].name = trans.ROLE_TEXT_+`${role.id}`;
            });

            observer.next(this.roles);
          });
        }
      }, error => {
          observer.next([]);
          this.error.manageSystemError(error);
      })
    });
  }

  getCountries() {
    return new Observable(observer => {
      this.getAllCountries().subscribe(reponse => {
        if (reponse && reponse.message === "success") {
            observer.next(reponse.countries);
        }
      }, error => {
          observer.next([]);
          this.error.manageSystemError(error);
      })
    });
  }

}
