import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from './ui.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorAuthService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }


    // Manage user error
    manageUserError(error) {
        if (error && error.error && error.error.user_not_found) {
          this.translate.get('BAD_CREDENTIALS_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.ancien_password_incorrect) {
          this.translate.get('OLD_PASSWORD_INCORRECT_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.remplir_tous_les_champs) {
          this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.country_id_not_exist) {
          this.translate.get('COUNTRY_NOT_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.phone_already_exist) {
          this.translate.get('PHONE_ALREADY_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        
        if (error && error.error && error.error.email_already_exist) {
          this.translate.get('EMAIL_ALREADY_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

            
        if (error && error.error && error.error.matricule_already_exist) {
          this.translate.get('MATRICULE_ALREADY_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 


        if (error && error.error && error.error.matricule_not_exist) {
          this.translate.get('MATRICULE_NOT_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.code_has_expired) {
          this.translate.get('CODE_HAS_EXPIRED_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        } 

        if (error && error.error && error.error.code_not_exist) {
          this.translate.get('CODE_NOT_EXIST_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        }

        
        if (error && error.error && error.error.user_not_confirm_code) {
          this.translate.get('CODE_NOT_CONFIRM_TEXT').subscribe(value => {
            this.ui.presentToast(value);
          });
        }
    }
}
