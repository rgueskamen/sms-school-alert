import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ApiService } from './api.service';
//import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';

@Injectable({
  providedIn: 'root'
})
export class PluginService {

  constructor(
    private camera: Camera,
    private api: ApiService,
   // private document: DocumentViewer
  ) { }

  // Get a picture
  getPicture(): Observable<any> {
    return new Observable((subscriber) => {
      const options: CameraOptions = {
        quality: 50,
        destinationType: this.camera.DestinationType.DATA_URL,
        mediaType: this.camera.MediaType.PICTURE,
        encodingType: this.camera.EncodingType.JPEG,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
      }
      this.camera.getPicture(options).then((imageData) => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        subscriber.next(base64Image);
      }, (err) => {
        subscriber.next(null);
      });

    })
  }

  // Get file
  getFileReader(): FileReader {
    const fileReader = new FileReader();
    const zoneOriginalInstance = (fileReader as any)["__zone_symbol__originalInstance"];
    return zoneOriginalInstance || fileReader;
  }
  
  // Get a file
  getFile(event): Observable<any> {
    return new Observable((subscriber) => {
    if (event.target.files && event.target.files[0]) {
      let newInstance = this.getFileReader();
      newInstance.readAsDataURL(event.target.files[0]);
      newInstance.onload = (imgsrc) => {
      let url = (imgsrc.target as FileReader).result;
      subscriber.next(url);
      }
    } else {
      subscriber.next(null);
    }

    });
  }

  // show File
  showFile(file: any, name ?:string) {
/*  
    const options: DocumentViewerOptions = {
      title: name ? name : 'My PDF'
    }
    this.document.viewDocument(file, 'application/pdf', options); */
  }

  showFileServer(file: any) {
    // this.showFile(this.api.baseUrl+file);
  }
}
