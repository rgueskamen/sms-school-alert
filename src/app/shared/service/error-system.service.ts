import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from './ui.service';
import { EventsService } from './events.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorSystemService {

  constructor(
    private translate: TranslateService,
    private authService: UserService,
    private ui: UiService,
    private message: EventsService
  ) { }
  
    // Renew the user session
    renewSession(showMessage?: boolean) {
      return new Promise((resolve) => {
        const credentials =  this.authService.getUserSecret();
        if (credentials) {
          this.authService.loginUser(credentials).subscribe(
            (reponse: any) => {
              if (reponse && reponse.message === 'success') {
                this.authService.setUserToken(reponse.token);
                this.authService.setUserData(reponse.user);
                this.authService.setUserSubscription(reponse.souscription);
                this.authService.setUserSecret(credentials);
                this.message.publish('token',reponse.token);
                setTimeout(() => {
                  if (showMessage) {
                     this.translate.get('TRY_AGAIN_TEXT').subscribe(value => {
                          this.ui.presentToast(value);
                    });
                  }
                  resolve({result: 'OK'});
                }, 20000);
              }
            }, error => {
             
              if (error && error.error && error.error.user_not_found) {
                this.authService.logout();
              } else {
                if (error.status === 0) {
                   this.translate.get('ERROR_INTERNET_TEXT').subscribe(value => {
                    this.ui.presentToast(value);
                   });
                }
              }
              resolve({result: 'ERROR'});
            });
        } else {
          resolve({result: 'ERROR'});
          this.authService.logout();
        }
  
      });
  
    }
  
    // Manage system error
    manageSystemError(error) {
      if (error.status === 0) {
        this.translate.get('ERROR_INTERNET_TEXT').subscribe(value => {
          this.ui.presentToast(value);
        });
      } else if (error.status === 400) {
        this.renewSession();
      } else if (error.status === 401) {
        this.authService.logout();
      } else if (error.status === 500) {
        this.translate.get('ERROR_SERVER_TEXT').subscribe(value => {
          this.ui.presentToast(value);
        });
      } else {
        this.translate.get('ERROR_SERVER_TEXT').subscribe(value => {
          this.ui.presentToast(value);
        });
      }
    }
}
