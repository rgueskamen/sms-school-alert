import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateserviceService {

  constructor(
    private translate: TranslateService
  ) { }

  getDateTimeUniversal(date: string, time: string, from: string) {

    const myDates = this.decodeDate(date);
    const myTimes = this.decodeTime(time);
    const objectMyDate = new Date(Number(myDates.year), Number(myDates.month - 1), Number(myDates.day), Number(myTimes.heure),
     Number(myTimes.minutes), 0, 0);
    let objectMyDateUniversal = {};

    if (from === 'l') {
      objectMyDateUniversal = new Date(objectMyDate.valueOf() + objectMyDate.getTimezoneOffset() * 60000);
    } else {
      objectMyDateUniversal = new Date(objectMyDate.valueOf() - objectMyDate.getTimezoneOffset() * 60000);
    }

    const dateFormater = this.formatterDate(objectMyDateUniversal);
    return dateFormater;

  }

  encodeTime(hour: string, minute: string) {
    const mTime = hour + ':' + minute;
    return mTime;
  }


  addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
  }

  decodeTime(time: string) {

    const times = time.split(':');
    const myTime = {
      heure: times[0],
      minutes: times[1],
      secondes: times[2],
    };

    return myTime;
  }


  decodeDate(myDate) {

    const mDates = myDate.split('-');
    const mDate = {
      year: mDates[0],
      month: mDates[1],
      day: mDates[2]

    };
    return mDate;
  }



  encodeDate(day: string, month: string, year: string) {
    const mDate = year + '-' + month + '-' + day;
    return mDate;
  }


  formatterDate(date: any) {

    const year = String(date.getFullYear());
    const month = String(('0' + (date.getMonth() + 1)).slice(-2));
    const day = String(('0' + date.getDate()).slice(-2));
    const hour = String(('0' + date.getHours()).slice(-2));
    const minute = String(('0' + date.getMinutes()).slice(-2));
    const myFormatDate = {
      date: year + '-' + month + '-' + day,
      time: hour + ':' + minute,
    };
    return myFormatDate;
  }

  formatDateSplash(date: any) {
    const dateFormat = new Date(date);
    const month = (dateFormat.getMonth() + 1 < 10) ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
    const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
    const formatDate = day + '/' + month + '/' +  dateFormat.getFullYear();
    return formatDate;
  }

  formatDateSplashReverse(date: any) {
    const dateFormat = new Date(date);
    const month = (dateFormat.getMonth() + 1 < 10) ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
    const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
    const formatDate =   dateFormat.getFullYear() + '/' + month + '/' +  day;
    return formatDate;
  }

  formatDateTiret(date: any) {
    const dateFormat = new Date(date);
    const month = (dateFormat.getMonth() + 1 < 10) ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
    const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
    const formatDate = dateFormat.getFullYear() + '-' + month + '-' + day;
    return formatDate;
  }

  formatDate(date: any) {
    const dateFormat = new Date(date);
    const month = (dateFormat.getMonth() + 1 < 10) ? '0' + (dateFormat.getMonth() + 1) : (dateFormat.getMonth() + 1);
    const day = dateFormat.getDate() < 10 ? '0' + dateFormat.getDate() : dateFormat.getDate();
    const formatDate = dateFormat.getFullYear() + '-' + month + '-' + day;
    return formatDate;
  }

  formatHeure(date: any) {
    const dateFormat = new Date(date);
    const hour = (dateFormat.getHours() + 1 < 10) ? '0' + dateFormat.getHours() : dateFormat.getHours();
    const minute = (dateFormat.getMinutes() + 1 < 10) ? '0' + dateFormat.getMinutes() : dateFormat.getMinutes();
    const seconde = (dateFormat.getSeconds() + 1 < 10) ? '0' + dateFormat.getSeconds() : dateFormat.getSeconds();
    const formatHeure = hour + ':' + minute + ':' + seconde;
    return formatHeure;
  }

  // Get Months list
  getMonths() {
    const monthsData = [];
    this.translate.get([
      'JANUARY_TEXT',
      'FEBRUARY_TEXT',
      'MARCH_TEXT',
      'APRIL_TEXT',
      'MAY_TEXT',
      'JUNE_TEXT',
      'JULY_TEXT',
      'AUGUST_TEXT',
      'SEPTEMBER_TEXT',
      'OCTOBER_TEXT',
      'NOVEMBER_TEXT',
      'DECEMBER_TEXT'
    ]).subscribe( trans => {
      monthsData.push({name : 'Janvier' , value :trans.JANUARY_TEXT});
      monthsData.push({name : 'Février' , value :trans.FEBRUARY_TEXT});
      monthsData.push({name : 'Mars' , value :trans.MARCH_TEXT});
      monthsData.push({name : 'Avril' , value :trans.APRIL_TEXT});
      monthsData.push({name : 'Mai' , value :trans.MAY_TEXT});
      monthsData.push({name : 'Juin' , value :trans.JUNE_TEXT});
      monthsData.push({name : 'Juillet' , value :trans.JULY_TEXT});
      monthsData.push({name : 'Août' , value :trans.AUGUST_TEXT});
      monthsData.push({name : 'Septembre' , value :trans.SEPTEMBER_TEXT});
      monthsData.push({name : 'Octobre' , value :trans.OCTOBER_TEXT});
      monthsData.push({name : 'Novembre' , value :trans.NOVEMBER_TEXT});
      monthsData.push({name : 'Décembre' , value :trans.DECEMBER_TEXT});
    });

    return monthsData;

  }

}
