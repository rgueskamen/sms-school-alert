import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ExpandableHeaderDirective } from './directives/expandable-header.directive';
import { FormsService } from './service/forms.service';
import { ApiService } from './service/api.service';
import { ConstantService } from './service/constant.service';
import { ErrorAuthService } from './service/error-auth.service';
import { ErrorSystemService } from './service/error-system.service';
import { EventsService } from './service/events.service';
import { DateserviceService } from './service/dateservice.service';
import { PluginService } from './service/plugin.service';
import { UiService } from './service/ui.service';
import { UtilsService } from './service/utils.service';
import { CountriesComponent } from './components/countries/countries.component';


@NgModule({
  declarations: [
    ExpandableHeaderDirective,
    CountriesComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports:[
    ExpandableHeaderDirective,
    CountriesComponent,
    CommonModule,
    FormsModule,
    HttpClientModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  providers:[
    TranslateService,
    FormsService,
    ApiService,
    ConstantService,
    ErrorAuthService,
    ErrorSystemService,
    EventsService,
    FormsService,
    DateserviceService,
    PluginService,
    UiService,
    UtilsService

  ]
})
export class SharedModule { }
