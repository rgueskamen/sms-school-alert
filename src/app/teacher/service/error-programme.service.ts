import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorProgrammeService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }


  // Manage planning error
  managePlanningError(error) {

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.programme_id_not_exist) {
      this.translate.get('PROGRAMM_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.matiere_id_not_exist) {
      this.translate.get('COURSE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    
    if (error && error.error && error.error.date_depot_examen_id_not_exist) {
      this.translate.get('DATE_EXAMEN_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

  }


}
