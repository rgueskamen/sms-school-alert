import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorAttendanceService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }

  // Manage attendance error
  manageAttendanceError(error) {

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.etablissement_scolaire_id_not_exist) {
      this.translate.get('SCHOOL_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.user_not_authorize) {
      this.translate.get('USER_NOT_AUTHORIZED').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.user_id_eleve_not_exist) {
      this.translate.get('USER_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.fiche_presence_id_not_exist) {
      this.translate.get('ATTENDANCE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

  }

}
