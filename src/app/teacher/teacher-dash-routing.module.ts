import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeacherDashPage } from './teacher-dash.page';

const routes: Routes = [
    {
        path: '',
        component: TeacherDashPage
    },
    { path: 'classes', loadChildren: () => import('./classes/classes.module').then(m => m.ClassesPageModule) },
    { path: 'program', loadChildren: () => import('./program/program-classes.module').then(m => m.ProgramClassesPageModule) },
    { path: 'attendance', loadChildren: () => import('./attendance/attendance.module').then( m => m.AttendancePageModule)},
    { path: 'check-attendance', loadChildren: () => import('./check-attendance/check-attendance.module').then( m => m.CheckAttendancePageModule)}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TeacherDashRoutingModule {

}
