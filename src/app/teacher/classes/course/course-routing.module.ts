import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursePage } from './course.page';

const routes: Routes = [
  {
    path: '',
    component: CoursePage
  },
  {
    path: 'new-program',
    loadChildren: () => import('./new-program/new-program.module').then(m => m.NewProgramPageModule)
  }
  ,
  {
    path: 'edit-program',
    loadChildren: () => import('./edit-program/edit-program.module').then(m => m.EditProgramPageModule)
  }
  ,
  {
    path: 'list-program',
    loadChildren: () => import('./list-program/list-program.module').then(m => m.ListProgramPageModule)
  },
  {
    path: 'textbook/:courseId',
    loadChildren: () => import('./fufill-textbook/fufill-textbook.module').then(m => m.FufillTextBookPageModule)
  },
  {
    path: 'textbook/view/:courseId',
    loadChildren: () => import('./view-textbook/view-textbook.module').then(m => m.ViewTextBookPageModule)
  },
  {
    path: 'new-test-date/:courseId',
    loadChildren: () => import('./add-test-date/add-test-date.module').then(m => m.AddTestDateDocPageModule)
  },
  {
    path: 'list-test-date/:courseId',
    loadChildren: () => import('./list-test-date/list-test-date.module').then(m => m.ListTestDatePageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsPageRoutingModule { }
