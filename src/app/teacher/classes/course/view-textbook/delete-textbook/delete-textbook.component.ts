import { Component, OnInit, Input } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { CoursService } from '../../../../service/cours.service';
import { ErrorCoursService } from '../../../../service/error-cours.service';

@Component({
  selector: 'app-delete-textbook',
  templateUrl: './delete-textbook.component.html',
  styleUrls: ['./delete-textbook.component.scss'],
})
export class DeleteTextBookComponent implements OnInit {

  textbookData: any;

  constructor(
    public popoverController: PopoverController,
    private course: CoursService,
    private courseError: ErrorCoursService,
    private translate: TranslateService,
    private ui: UiService,
    private errorService: ErrorSystemService,
    private navParams: NavParams
  ) { 
      this.textbookData = this.navParams.get('textbook');
  }

  ngOnInit() {}

  close(ans ?: any) {
    this.popoverController.dismiss(ans, null);
  }

  // Delete the textbook
  deleteTextbook() {
    const data = { cahier_texte_id : this.textbookData.cahier_texte_id};
    this.translate.get('DELETE_LOADING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });
    this.course.deleteTextBookInfo(data)
      .subscribe((reponse: any) => {
        this.ui.dismissLoading();
        if (reponse && reponse.message ==="success") {
            this.close('deleted');
            this.translate.get('TEXTBOOK_DELETE_TEXT').subscribe(trans => {
              this.ui.presentToast(trans);
            });
        }
      }, error => {
        if (error && error.error && error.error.message === "error") {
          if (error && error.error && error.error.user_not_found) {
              this.errorService.renewSession().then((data: any) => {
                    this.ui.dismissLoading();
                    if (data && data.result === "OK") {
                        this.deleteTextbook();
                    } else {
                      this.close();
                    }
              });
          } else {
            this.close();
            this.ui.dismissLoading();
            this.courseError.manageCoursesError(error);
          }
        } else {
          this.close();
          this.ui.dismissLoading();
            this.errorService.manageSystemError(error);
        }
      });
  }

}
