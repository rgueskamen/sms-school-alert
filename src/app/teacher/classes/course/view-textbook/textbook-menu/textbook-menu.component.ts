import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { EventsService } from 'src/app/shared/service/events.service';
import { DeleteTextBookComponent } from '../delete-textbook/delete-textbook.component';
import { EditTextBookPage } from '../edit-textbook/edit-textbook.page';
import { StudentAdvisePage } from '../student-advise/student-advise.page';

@Component({
  selector: 'app-textbook-menu',
  templateUrl: './textbook-menu.component.html',
  styleUrls: ['./textbook-menu.component.scss'],
})
export class TextBookMenuComponent implements OnInit {
  textbook: any;
  courseID: any;
  classID: any;
  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private event: EventsService,
    private modatCtrl: ModalController
  ) {
    this.textbook = this.navParams.get('textbook');
    this.courseID = this.navParams.get('courseId');
    this.classID = this.navParams.get('classId');
  }

  ngOnInit() { }

  closeMenu() {
    this.popoverController.dismiss();
  }


  // Open the modal
  editTextBook() {
    this.closeMenu();
    this.modatCtrl
      .create({
        component: EditTextBookPage,
        componentProps: {
          textbook: this.textbook,
          courseID: this.courseID,
          classID: this.classID
        }
      })
      .then(modalEl => {
        modalEl.present();
        modalEl.onDidDismiss().then(() => {
          // make update
        });
      });
  }


  // View the student advise
  studentAdvise() {
    this.closeMenu();
    this.modatCtrl
      .create({
        component: StudentAdvisePage,
        componentProps: {
          textbook: this.textbook,
          classId: this.classID
        }
      })
      .then(modalEl => {
        modalEl.present();
        modalEl.onDidDismiss().then(() => {
          // make update
        });
      });
  }



    // Delete the swap request
    async delteTextBook() {
      this.closeMenu();
      const popover = await this.popoverController.create({
        component: DeleteTextBookComponent,
        cssClass: 'delete-popover',
        componentProps: {
          textbook: this.textbook 
        }
      });
  
      popover.onDidDismiss().then((ans) => {
        if (ans && ans.data === "deleted") {
          this.event.publish('update-text-book');
         }
      });
      return await popover.present();
    }




}
