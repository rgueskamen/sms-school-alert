import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ClasseServiceService } from 'src/app/school/service/classe-service.service';
import { ErrorClassService } from 'src/app/school/service/error-class.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { ClassMenuComponent } from './class-menu/class-menu.component';
import { CourseMenuComponent } from './course-menu/course-menu.component';

@Component({
  selector: 'app-cours',
  templateUrl: 'course.page.html',
  styleUrls: ['course.page.scss']
})
export class CoursePage {
  courses: any[] = [];
  classeId: number;
  loading: boolean;

  constructor(
    private classe: ClasseServiceService,
    private classError: ErrorClassService,
    private error: ErrorSystemService,
    private popoverController: PopoverController,
    private activeRoute: ActivatedRoute
  ) { 
      this.loading = true;
      this.classeId = this.activeRoute.snapshot.params.classId;
  }

  ngOnInit(): void {
    this.getCoursesList(null);
  }

  doRefresh(event) {
    this.getCoursesList(event);
  }

  // format the course reponse data
  formatCourseResponseData(data: any) {
      if (data && data.message === "success") {
        this.courses = data.matieres;
      }
  }

  // Get the course list
  getCoursesList(event: any) {
    this.classe.getCoursesOfSchoolClasses(this.classeId).subscribe((reponse: any) => {
      this.formatCourseResponseData(reponse);
      this.loading = false;
      if (event) {
          setTimeout(() => {
            event.target.complete();
          },200);
      }
    }, error => {

      this.loading = false;
      if (event) {
          event.target.complete();
       }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.loading = true;
          this.error.renewSession().then((data: any) => {
                if (data && data.result === "OK") {
                  this.getCoursesList(event);
                } else {
                  this.loading = false;
                }
          });
        
        } else {
          this.classError.manageClassError(error);
        }

      } else {
          this.error.manageSystemError(error);
      }

    });
  }

  // open the teachear menu
  async openMenu(ev, courseData: any) {
    const popover = await this.popoverController.create({
      component: CourseMenuComponent,
      event: ev,
      componentProps:{
        course: courseData,
        classId: this.classeId
      }
    });
    return await popover.present();
  }

      // open the teachear menu
      async openClassMenu(ev) {
        const popover = await this.popoverController.create({
          component: ClassMenuComponent,
          event: ev,
          componentProps:{
            classId: this.classeId
          }
        });
        return await popover.present();
      }
}
