import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddTestDatePage } from './add-test-date.page';

const routes: Routes = [
  {
    path: '',
    component: AddTestDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddTestDatePageRoutingModule {}
