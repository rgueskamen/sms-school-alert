import { Component,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DateserviceService } from 'src/app/shared/service/dateservice.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { ErrorProgrammeService } from '../../../service/error-programme.service';
import { ProgrammeService } from '../../../service/programme-service.service';

@Component({
  selector: 'app-add-test-date',
  templateUrl: './add-test-date.page.html',
  styleUrls: ['./add-test-date.page.scss'],
})
export class AddTestDatePage implements OnInit {

  dataForm: FormGroup;
  validationMessages: any;
  loading: boolean;
  classId: any;
  courseId: any;

  constructor(
    private fb: FormBuilder,
    private event: EventsService,
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private date: DateserviceService,
    private activated: ActivatedRoute,
    private ui: UiService,
    private translate: TranslateService,
    private system: ErrorSystemService,
  ) {
      this.loading = false;
      this.classId = this.activated.snapshot.params.classId;
      this.courseId = this.activated.snapshot.params.courseId;
   }

  ngOnInit() {
    this.initFormData();
    this.validationMessage();
  }


  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        matiere_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        ,
        date_debut: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        ,
        heure_debut: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        ,
        date_fin: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        ,
        heure_fin: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]

      };
    });
  }

  // Form getters
  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  get matiere_id() {
    return this.dataForm.get('matiere_id');
  }

  get date_debut() {
    return this.dataForm.get('date_debut');
  }

  get heure_debut() {
    return this.dataForm.get('heure_debut');
  }

  get date_fin() {
    return this.dataForm.get('date_fin');
  }

  get heure_fin() {
    return this.dataForm.get('heure_fin');
  }

  // Init the form 
  initFormData() {
    this.dataForm = this.fb.group({
      classe_id: [this.classId, Validators.required],
      matiere_id: [this.courseId, Validators.required],
      date_debut: ['', Validators.required],
      heure_debut: ['', Validators.required],
      date_fin: ['', Validators.required],
      heure_fin: ['', Validators.required]
    });
  }

  // new programm
  addTestDateProgram() {
    this.loading = true;
    let param = this.dataForm.value;
    param.date_debut = this.date.formatDate(param.date_debut);
    param.heure_debut = this.date.formatHeure(param.heure_debut);
    param.date_fin = this.date.formatDate(param.date_fin);
    param.heure_fin = this.date.formatHeure(param.heure_fin);
    this.program.saveProgrammTestDate(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('PROGRAM_DATE_SAVE_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.event.publish('new-program');
        }
      }, error => {
     
        if (error && error.error && error.error.message === "error") {

          if (error.error.user_not_found) {
            this.system.renewSession().then((data:any) => {
                  if (data && data.result === "OK") {
                      this.addTestDateProgram();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.programError.managePlanningError(error);
          }
       
        } else {
          this.loading = false;
          this.system.manageSystemError(error);
        }
    });
  }

}
