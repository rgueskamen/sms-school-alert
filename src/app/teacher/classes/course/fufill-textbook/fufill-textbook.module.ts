import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FuffillTextBookPageRoutingModule } from './fufill-textbook-routing.module';

import { SharedModule } from 'src/app/shared/shared.module';
import { FufillTextBookPage } from './fufill-textbook.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    FuffillTextBookPageRoutingModule
  ],
  declarations: [FufillTextBookPage]
})
export class FufillTextBookPageModule {}
