import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-class-menu',
  templateUrl: './class-menu.component.html',
  styleUrls: ['./class-menu.component.scss'],
})
export class ClassMenuComponent implements OnInit {
  course: any;
  classeId: any;
  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private router: Router
  ) { 
    this.course = this.navParams.get('course');
    this.classeId =  this.navParams.get('classId');
  }

  ngOnInit() { }
  
    closeCourseMenu() {
    this.popoverController.dismiss();
  }


  // new programm
  newProgram() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','new-program']);
  }

  // View programm
  viewProgram() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','list-program']);
  }

  // Edit the programm
  editProgram() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','edit-program']);
  }



}
