import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListTestDatePage } from './list-test-date.page';

const routes: Routes = [
  {
    path: '',
    component: ListTestDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListTestDatePageRoutingModule {}
