import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { SharedModule } from 'src/app/shared/shared.module';
import { ListProgramPageRoutingModule } from './list-program-routing.module';
import { ListProgramPage } from './list-program.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    ListProgramPageRoutingModule
  ],
  declarations: [ListProgramPage]
})
export class ListProgramPageModule {}
