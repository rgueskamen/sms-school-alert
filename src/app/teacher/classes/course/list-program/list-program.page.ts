import { Component, OnInit } from '@angular/core';
import { ClasseServiceService } from 'src/app/school/service/classe-service.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { ErrorClassService } from 'src/app/school/service/error-class.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { PluginService } from 'src/app/shared/service/plugin.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { ProgrammeService } from '../../../service/programme-service.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { ErrorProgrammeService } from '../../../service/error-programme.service';

@Component({
  selector: 'app-list-program',
  templateUrl: './list-program.page.html',
  styleUrls: ['./list-program.page.scss'],
})
export class ListProgramPage implements OnInit {

  classes: any;
  loading: boolean;
  classId: any;
  programs: any;
  selectedProgram: any;
  activeProgram: any;

  constructor(
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private error: ErrorSystemService,
    private plugin: PluginService,
    private translate: TranslateService,
    private activated: ActivatedRoute,
    private utils: UtilsService,
    private ui: UiService
  ) {
    this.programs = [];
    this.selectedProgram = [];
    this.loading = true;
    this.classId = this.activated.snapshot.params.classId;
  }

  ngOnInit() {
    this.getActiveProgram();
    this.getProgramList(null);
  }

  doRefresh(event) {
    this.getActiveProgram();
    this.getProgramList(event);
  }

  // update the selected programm
  updateSelectProgram(data: any) {
    const param = {programme_id: data.id, active: data.active === 1 ? 0 : 1 };
    this.program.activateOrDisableTestPlanning(param).subscribe((reponse: any) => {
      if (reponse && reponse.message === "success") {
       this.getProgramList(null);
      }
    }, error => {
        if (error && error.error && error.error.message === "error") {
          if (error.error.user_not_found) {
              this.error.renewSession().then((data: any)=> {
                  if (data && data.result === "OK") {
                    this.updateSelectProgram(data);
                  }
              });
          } else {
            this.programError.managePlanningError(error);
          }
        } else {
         this.error.manageSystemError(error);
        }
    });
  }

  // format the course reponse data
  formatProgramResponseData(data: any) {
    if (data && data.message === "success") {
      this.programs = data.programme;
      this.programs.forEach(prog => {
        prog.select = prog.active === 1 ? true: false;
      });
      this.programs =  this.utils.orderByKeyUp(this.programs,'active');
    }
  }

  // Get the program list
  getProgramList(event: any) {
    this.program.getClassProgram(this.classId).subscribe((reponse: any) => {
      this.formatProgramResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getProgramList(event);
            } else {
              this.loading = false;
            }
          });
        } else {
          this.loading = false;
          this.programError.managePlanningError(error);
        }
      } else {
        this.loading = false;
        this.error.manageSystemError(error);
      }
    });
  }

  getActiveProgram() {
    this.program.getActivePlanning(this.classId).subscribe((reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.activeProgram = reponse.programme;
          console.log(this.activeProgram);
        }
    }, error => {
   
      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getActiveProgram();
            }
          });
        } else {
          this.programError.managePlanningError(error);
        }
      } else {
        this.error.manageSystemError(error);
      }
    });
  }

  // view program
  viewProgram(data: any) {
    if (data.programme) {
      this.plugin.showFileServer(data.programme);
    } else {
      this.translate.get('NONE_DOCUMENT').subscribe(trans => {
        this.ui.presentToast(trans);
      });
    }
  }


}
