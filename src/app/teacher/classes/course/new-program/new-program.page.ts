import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { PluginService } from 'src/app/shared/service/plugin.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { ErrorProgrammeService } from '../../../service/error-programme.service';
import { ProgrammeService } from '../../../service/programme-service.service';

@Component({
  selector: 'app-new-program',
  templateUrl: './new-program.page.html',
  styleUrls: ['./new-program.page.scss'],
})
export class NewProgramPage implements OnInit {

  dataForm: FormGroup;
  validationMessages: any;
  loading: boolean;
  classId: any;
  @ViewChild('fileInput',{static: false}) fileInputClick: ElementRef;

  constructor(
    private fb: FormBuilder,
    private plugin: PluginService,
    private event: EventsService,
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private activated: ActivatedRoute,
    private ui: UiService,
    private translate: TranslateService,
    private system: ErrorSystemService,
    private zone: NgZone
  ) {
      this.loading = false;
      this.classId = this.activated.snapshot.params.classId;
   }

  ngOnInit() {
    this.initFormData();
    this.validationMessage();
  }

  buttonClick() {
    this.fileInputClick.nativeElement.click();    
  }

  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        programme: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]

      };
    });
  }

  // Form getters
  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  get programme() {
    return this.dataForm.get('programme');
  }

  // Init the form 
  initFormData() {
    this.dataForm = this.fb.group({
      classe_id: [this.classId, Validators.required],
      programme: ['', Validators.required]
    });
  }

  // Get the documents file
  getFile(event) {
      this.plugin.getFile(event).subscribe((file: any) => {
        if (file) {
          this.zone.run(() => {
            this.dataForm.get('programme').setValue(file);
          });
        }
      });
    }

  // new programm
  newProgram() {
    this.loading = true;
    let param = this.dataForm.value;
    console.log(param);
    this.program.saveTestPlanning(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('PROGRAM_SAVE_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.event.publish('new-program');
        }
      }, error => {
     
        if (error && error.error && error.error.message === "error") {

          if (error.error.user_not_found) {
            this.system.renewSession().then((data:any) => {
                  if (data && data.result === "OK") {
                      this.newProgram();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.programError.managePlanningError(error);
          }
       
        } else {
          this.loading = false;
          this.system.manageSystemError(error);
        }
    });
  }

}
