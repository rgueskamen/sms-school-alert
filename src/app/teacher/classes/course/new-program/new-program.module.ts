import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewProgramPage } from './new-program.page';
import { NewProgramPageRoutingModule } from './new-program-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    NewProgramPageRoutingModule
  ],
  declarations: [NewProgramPage]
})
export class NewProgramPageModule {}
