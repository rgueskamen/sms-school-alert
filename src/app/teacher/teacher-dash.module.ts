import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { TeacherDashRoutingModule } from './teacher-dash-routing.module';
import { TeacherDashPage } from './teacher-dash.page';
import { TeacherMenuComponent } from './teacher-menu/teacher-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    TeacherDashRoutingModule
  ],
  declarations: [
    TeacherDashPage,
    TeacherMenuComponent
  ],
  entryComponents: [
    TeacherMenuComponent
  ]
})
export class TeacherDashPageModule {}
