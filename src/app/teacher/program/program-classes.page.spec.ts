import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProgramClassesPage } from './program-classes.page';

describe('ClassesPage', () => {
  let component: ProgramClassesPage;
  let fixture: ComponentFixture<ProgramClassesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramClassesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProgramClassesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
