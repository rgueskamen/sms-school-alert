import { Component, OnInit } from '@angular/core';
import { ClasseServiceService } from 'src/app/school/service/classe-service.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UserService } from 'src/app/user/service/user-service.service';
import { ErrorClassService } from 'src/app/school/service/error-class.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-program-classes',
  templateUrl: './program-classes.page.html',
  styleUrls: ['./program-classes.page.scss'],
})
export class ProgramClassesPage implements OnInit {

  classes: any;
  loading: boolean;

  constructor(
    private classe: ClasseServiceService,
    private userService: UserService,
    private error: ErrorSystemService,
    private classError: ErrorClassService,
    private router: Router
  ) {
    this.classes = [];
    this.loading = true;
  }

  ngOnInit() {
    this.getClassesList(null);
  }


  doRefresh(event) {
    this.getClassesList(event);
  }

  // format the course reponse data
  formatClassesResponseData(data: any) {
    if (data && data.message === "success") {
      this.classes = data.classes;
      console.log(this.classes);
    }
  }

  // Get the course list
  getClassesList(event: any) {
    const currentUser = this.userService.getUserData();
    console.log(currentUser);
    this.classe.getSchoolClasses(currentUser.etablissement_scolaire_id).subscribe((reponse: any) => {
      this.formatClassesResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {

      this.loading = false;
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.loading = true;
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getClassesList(event);
            } else {
              this.loading = false;
            }
          });

        } else {
          this.classError.manageClassError(error);
        }

      } else {
        this.error.manageSystemError(error);
      }

    });
  }

  // go to the list of courses
  getProgram(classId: number){
      console.log(classId);
      this.router.navigate(['teacher','program',classId,'program-detail']);
  }

}
