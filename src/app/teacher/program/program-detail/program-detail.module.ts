import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { SharedModule } from 'src/app/shared/shared.module';
import { ProgramPageRoutingModule } from './program-detail-routing.module';
import { ProgramDetailPage } from './program-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    ProgramPageRoutingModule
  ],
  declarations: [ProgramDetailPage]
})
export class ProgramDetailPageModule {}
