import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProgramClassesPage } from './program-classes.page';

const routes: Routes = [
  { path: '', component: ProgramClassesPage },
  { path: ':classId/program-detail',
  loadChildren: () => import('./program-detail/program-detail.module').then(m => m.ProgramDetailPageModule) }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProgramClassesPageRoutingModule {}
