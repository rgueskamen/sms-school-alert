import { NewPeriodeComponent } from './new-periode/new-periode.component';
import { PopoverController, ModalController } from '@ionic/angular';
import { MenuAttendanceComponent } from './menu-attendance/menu-attendance.component';
import { UtilsService } from './../../../shared/service/utils.service';
import { UserService } from './../../../user/service/user-service.service';
import { ErrorSystemService } from './../../../shared/service/error-system.service';
import { ErrorAttendanceService } from './../../service/error-attendance.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AttendanceService } from './../../service/attendance.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendance-periode',
  templateUrl: './attendance-periode.page.html',
  styleUrls: ['./attendance-periode.page.scss'],
})
export class AttendancePeriodePage implements OnInit {
  classId: number;
  loading: boolean;
  attendancePeriode: any;

  constructor(
    private attendance: AttendanceService,
    public popoverController: PopoverController,
    private router: Router,
    private modalCtrl: ModalController,
    private activatedRoute: ActivatedRoute,
    private util: UtilsService,
    private userService: UserService,
    private errorAttendance: ErrorAttendanceService,
    private error: ErrorSystemService
  ) {
      this.classId = this.activatedRoute.snapshot.params.classId;
      this.loading = false;
      this.attendancePeriode = [];
  }

  ngOnInit() {
    this.getAttendancePeriodeList(null);
  }

  // refresh the page
  doRefresh(event: any) {
    this.getAttendancePeriodeList(event);
  }

    // Format the attendance response
    formatAttendanceResponseData(reponse: any) {
      if (reponse && reponse.message === 'success') {
        this.attendancePeriode = this.util.orderPropertyByKeyDown(reponse.liste_fiche,'fiche_presence','active');
        this.attendancePeriode.forEach(attend=> {
          attend.select = attend.active === 1 ? true: false;
        });
        console.log(this.attendancePeriode);
      }
    }

    // Get the attendance periode
    getAttendancePeriodeList(event: any) {
      const currentUser = this.userService.getUserData();
      console.log(currentUser);
      this.attendance.getAllAttendance(currentUser.etablissement_scolaire_id,this.classId).subscribe((reponse: any) => {
        console.log(reponse);
        this.formatAttendanceResponseData(reponse);
        this.loading = false;
        if (event) {
          setTimeout(() => {
            event.target.complete();
          }, 200);
        }
      }, error => {
        if (event) {
          event.target.complete();
        }
        if (error && error.error && error.error.message === 'error') {
          if (error && error.error && error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === 'OK') {
                this.getAttendancePeriodeList(event);
              } else {
                this.loading = false;
              }
            });
          } else {
            this.loading = false;
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
          this.loading = false;
          this.error.manageSystemError(error);
        }
      });
    }


      // open the teachear menu
  async openMenu(ev, attendancePeriode: any) {
    const popover = await this.popoverController.create({
      component: MenuAttendanceComponent,
      event: ev,
      componentProps:{
        period: attendancePeriode,
        classId: this.classId
      }
    });
    return await popover.present();
  }

  // new attendance time
async  newAttendanceTime() {
    const currentUser = this.userService.getUserData();
    const modal = await this.modalCtrl.create({
      component: NewPeriodeComponent,
      componentProps:{
        classId: this.classId,
        schoolId: currentUser.etablissement_scolaire_id
      }
    });
    modal.present();
    modal.onDidDismiss().then(() => {
      this.getAttendancePeriodeList(null);
    });
  }

  detailAttendance(attendance: any) {
    this.router.navigate(['teacher/attendance/attendance-peroide',this.classId,'list-attendance', attendance.fiche_presence.id]);
  }

  // update the selected periode
  updateSelectedAttendancePeriod(data: any) {
    const param = {fiche_presence_id: data.id, active: data.active === 1 ? 0 : 1 };
    this.attendance.enableOrDisableAttendance(param).subscribe((reponse: any) => {
      if (reponse && reponse.message === 'success') {
       this.getAttendancePeriodeList(null);
      }
    }, error => {
        if (error && error.error && error.error.message === 'error') {
          if (error.error.user_not_found) {
              this.error.renewSession().then((ans: any)=> {
                  if (ans && ans.result === 'OK') {
                    this.updateSelectedAttendancePeriod(data);
                  }
              });
          } else {
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
         this.error.manageSystemError(error);
        }
    });
  }

}
