import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AttendancePeriodePage } from './attendance-periode.page';

const routes: Routes = [
  {
    path: '',
    component: AttendancePeriodePage
  },
  {
    path: 'attendance-detail/:attendanceId',
    loadChildren: () => import('./attendance-list/attendance-list.module').then( m => m.AttendanceListPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AttendancePeriodePageRoutingModule {}
