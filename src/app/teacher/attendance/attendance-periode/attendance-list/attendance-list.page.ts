import { ErrorSystemService } from './../../../../shared/service/error-system.service';
import { ErrorAttendanceService } from './../../../service/error-attendance.service';
import { UserService } from './../../../../user/service/user-service.service';
import { UtilsService } from './../../../../shared/service/utils.service';
import { ActivatedRoute } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AttendanceService } from './../../../../parent/service/attendance.service';
import { EditPeriodeComponent } from './../edit-periode/edit-periode.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendance-list',
  templateUrl: './attendance-list.page.html',
  styleUrls: ['./attendance-list.page.scss'],
})
export class AttendanceListPage implements OnInit {

  attendanceId: number;
  loading: boolean;
  attendanceDetail: any;
  observations: any;
  studentAttendances: any;

  constructor(
    private attendance: AttendanceService,
    private modalCtrl: ModalController,
    private activatedRoute: ActivatedRoute,
    private util: UtilsService,
    private userService: UserService,
    private errorAttendance: ErrorAttendanceService,
    private error: ErrorSystemService
  ) {
      this.attendanceId = this.activatedRoute.snapshot.params.attendanceId;
      this.loading = false;
      this.attendanceDetail = [];
      this.observations = [];
      this.studentAttendances = [];
  }

  ngOnInit() {
    this.getAttendanceDetailList(null);
  }

  // refresh the page
  doRefresh(event: any) {
    this.getAttendanceDetailList(event);
  }

    // Format the attendance response
    formatAttendanceResponseData(reponse: any) {
      if (reponse && reponse.message === 'success') {
        this.attendanceDetail = this.util.orderPropertyByKeyDown(reponse.liste_fiche,'fiche_presence','active');
        this.observations = this.util.orderByKeyDown(reponse.observations,'date_du_jour');
        this.studentAttendances = this.util.orderByKeyDown(reponse.detail_fiche_presence,'date_du_jour');;
      }
    }

    // Get the attendance periode
    getAttendanceDetailList(event: any) {
      this.attendance.getAttendanceInfo(this.attendanceId).subscribe((reponse: any) => {
        console.log(reponse);
        this.formatAttendanceResponseData(reponse);
        this.loading = false;
        if (event) {
          setTimeout(() => {
            event.target.complete();
          }, 200);
        }
      }, error => {
        if (event) {
          event.target.complete();
        }
        if (error && error.error && error.error.message === 'error') {
          if (error && error.error && error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === 'OK') {
                this.getAttendanceDetailList(event);
              } else {
                this.loading = false;
              }
            });
          } else {
            this.loading = false;
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
          this.loading = false;
          this.error.manageSystemError(error);
        }
      });
    }


  // new attendance time
async  updatePresence(data: any) {
    const currentUser = this.userService.getUserData();
    const modal = await this.modalCtrl.create({
      component: EditPeriodeComponent,
      componentProps:{
        student: data
      }
    });
    modal.present();
    modal.onDidDismiss().then(() => {
      this.getAttendanceDetailList(null);
    });
  }
}
