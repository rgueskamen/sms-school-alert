import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AttendancePeriodePage } from './attendance-periode.page';

describe('AttendancePeriodePage', () => {
  let component: AttendancePeriodePage;
  let fixture: ComponentFixture<AttendancePeriodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendancePeriodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AttendancePeriodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
