import { EventsService } from './../../../../shared/service/events.service';
import { UiService } from './../../../../shared/service/ui.service';
import { DateserviceService } from 'src/app/shared/service/dateservice.service';
import { ErrorSystemService } from './../../../../shared/service/error-system.service';
import { ErrorAttendanceService } from './../../../service/error-attendance.service';
import { AttendanceService } from './../../../service/attendance.service';
import { TranslateService } from '@ngx-translate/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-periode',
  templateUrl: './edit-periode.component.html',
  styleUrls: ['./edit-periode.component.scss'],
})
export class EditPeriodeComponent implements OnInit {

  validationMessages: any;
  dataForm: FormGroup;
  loading: boolean;
  monthsList: any;
  data:any;
  constructor(
    private navParams: NavParams,
    private translate: TranslateService,
    private modalCtl: ModalController,
    private fb : FormBuilder,
    private attendace: AttendanceService,
    private errorAttendance: ErrorAttendanceService,
    private system: ErrorSystemService,
    private date: DateserviceService,
    private ui: UiService,
    private event: EventsService
  ) {
      this.data = this.navParams.get('period');
      this.loading = false;
      this.monthsList = this.date.getMonths();
  }

  ngOnInit() {
    this.initFormData();
    this.validationMessage();
  }


  // close the modal
  closModal() {
    this.modalCtl.dismiss();
  }

  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        etablissement_scolaire_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        mois: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        semaine_start: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        semaine_end: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }

  // Form getters
  get etablissement_scolaire_id() {
    return this.dataForm.get('etablissement_scolaire_id');
  }

  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  get mois() {
    return this.dataForm.get('mois');
  }

  get semaine_start() {
    return this.dataForm.get('semaine_start');
  }

  get semaine_end() {
    return this.dataForm.get('semaine_end');
  }

  // Init the form
  initFormData() {
    this.dataForm = this.fb.group({
      etablissement_scolaire_id: [this.data.infos_etablissement_scolaire.id, Validators.required],
      classe_id: [this.data.infos_classe.id, Validators.required],
      mois: [this.data.fiche_presence.mois, Validators.required],
      semaine_start: [this.date.formatDateTiret(this.data.fiche_presence.semaine_start), Validators.required],
      semaine_end: [this.date.formatDateTiret(this.data.fiche_presence.semaine_end), Validators.required]
    });
  }

  // save attendance periode
  saveAttendancePeriode() {
    this.loading = true;
    const param = this.dataForm.value;
    param.semaine_start = this.date.formatDate(param.semaine_start);
    param.semaine_end = this.date.formatDate(param.semaine_end);
    this.attendace.updateAttendance(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('PERIODE_ATTENDANCE_EDIT_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.event.publish('new-periode');
          this.closModal();
        }
      }, error => {
        if (error && error.error && error.error.message === 'error') {
          if (error.error.user_not_found) {
            this.system.renewSession().then((data:any) => {
                  if (data && data.result === 'OK') {
                      this.saveAttendancePeriode();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
          this.loading = false;
          this.system.manageSystemError(error);
        }
    });
  }

}
