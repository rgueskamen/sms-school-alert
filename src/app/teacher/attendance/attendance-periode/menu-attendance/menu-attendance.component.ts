import { EventsService } from './../../../../shared/service/events.service';
import { EditPeriodeComponent } from './../edit-periode/edit-periode.component';
import { Router } from '@angular/router';
import { NavParams, PopoverController, ModalController } from '@ionic/angular';
import { ViewTextBookPage } from './../../../classes/course/view-textbook/view-textbook.page';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu-attendance',
  templateUrl: './menu-attendance.component.html',
  styleUrls: ['./menu-attendance.component.scss'],
})
export class MenuAttendanceComponent implements OnInit {

  classId: number;
  period: any;

  constructor(
    private params: NavParams,
    private popoverController: PopoverController,
    private modalCtrl: ModalController,
    private event: EventsService,
    private router: Router
  ) {
    this.classId =  this.params.get('classId');
    this.period = this.params.get('period');
  }

  ngOnInit() {}


  closeCourseMenu() {
    this.popoverController.dismiss();
  }

  // View
  ViewTextBookPage() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/attendance/attendance-peroide',this.classId,'list-attendance']);
  }

  // Edit
  async edit() {
  this.closeCourseMenu();
      // new attendance time
  const modal = await this.modalCtrl.create({
    component: EditPeriodeComponent,
    componentProps:{
      classId: this.classId,
      period: this.period
    }
  });
  modal.present();
  modal.onDidDismiss().then(() => {
    this.event.publish('new-periode');
  });
  }

}
