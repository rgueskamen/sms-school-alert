import { MenuAttendanceComponent } from './menu-attendance/menu-attendance.component';
import { EditPeriodeComponent } from './edit-periode/edit-periode.component';
import { NewPeriodeComponent } from './new-periode/new-periode.component';
import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttendancePeriodePageRoutingModule } from './attendance-periode-routing.module';

import { AttendancePeriodePage } from './attendance-periode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    AttendancePeriodePageRoutingModule
  ],
  declarations: [AttendancePeriodePage,
    NewPeriodeComponent,
    EditPeriodeComponent,
    MenuAttendanceComponent
  ],
  entryComponents:[
    NewPeriodeComponent,
    EditPeriodeComponent,
    MenuAttendanceComponent
  ]
})
export class AttendancePeriodePageModule {}
