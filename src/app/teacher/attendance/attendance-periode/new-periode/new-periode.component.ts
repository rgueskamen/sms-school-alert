import { EventsService } from './../../../../shared/service/events.service';
import { UiService } from './../../../../shared/service/ui.service';
import { ErrorSystemService } from './../../../../shared/service/error-system.service';
import { ErrorAttendanceService } from './../../../service/error-attendance.service';
import { AttendanceService } from './../../../../parent/service/attendance.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { NavParams, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DateserviceService } from 'src/app/shared/service/dateservice.service';

@Component({
  selector: 'app-new-periode',
  templateUrl: './new-periode.component.html',
  styleUrls: ['./new-periode.component.scss'],
})
export class NewPeriodeComponent implements OnInit {

  classId: number;
  schoolId: any;
  validationMessages: any;
  dataForm: FormGroup;
  loading: boolean;
  monthsList: any;
  constructor(
    private navParams: NavParams,
    private translate: TranslateService,
    private modalCtl: ModalController,
    private fb : FormBuilder,
    private attendace: AttendanceService,
    private errorAttendance: ErrorAttendanceService,
    private system: ErrorSystemService,
    private date: DateserviceService,
    private ui: UiService,
    private event: EventsService
  ) {
      this.classId = this.navParams.get('classId');
      this.schoolId = this.navParams.get('schoolId');
      this.loading = false;
      this.monthsList = this.date.getMonths();
  }

  ngOnInit() {
    this.initFormData();
    this.validationMessage();
  }


  // close the modal
  closModal() {
    this.modalCtl.dismiss();
  }

  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        etablissement_scolaire_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        mois: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        semaine_start: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        semaine_end: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }

  // Form getters
  get etablissement_scolaire_id() {
    return this.dataForm.get('etablissement_scolaire_id');
  }

  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  get mois() {
    return this.dataForm.get('mois');
  }

  get semaine_start() {
    return this.dataForm.get('semaine_start');
  }

  get semaine_end() {
    return this.dataForm.get('semaine_end');
  }

  // Init the form
  initFormData() {
    this.dataForm = this.fb.group({
      etablissement_scolaire_id: [this.schoolId , Validators.required],
      classe_id: [this.classId, Validators.required],
      mois: ['', Validators.required],
      semaine_start: ['', Validators.required],
      semaine_end: ['', Validators.required]
    });
  }

  // save attendance periode
  saveAttendancePeriode() {
    this.loading = true;
    const param = this.dataForm.value;
    param.semaine_start = this.date.formatDate(param.semaine_start);
    param.semaine_end = this.date.formatDate(param.semaine_end);
    this.attendace.saveAttendance(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('PERIDE_ATTENDANCE_SAVE_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.event.publish('new-periode');
          this.closModal();
        }
      }, error => {
        if (error && error.error && error.error.message === 'error') {
          if (error.error.user_not_found) {
            this.system.renewSession().then((data:any) => {
                  if (data && data.result === 'OK') {
                      this.saveAttendancePeriode();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
          this.loading = false;
          this.system.manageSystemError(error);
        }
    });
  }

}
