import { Router } from '@angular/router';
import { ErrorClassService } from './../../school/service/error-class.service';
import { ErrorSystemService } from './../../shared/service/error-system.service';
import { UserService } from './../../user/service/user-service.service';
import { ClasseServiceService } from './../../school/service/classe-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.page.html',
  styleUrls: ['./attendance.page.scss'],
})
export class AttendancePage implements OnInit {
  classes: any;
  loading: boolean;

  constructor(
    private classe: ClasseServiceService,
    private userService: UserService,
    private error: ErrorSystemService,
    private classError: ErrorClassService,
    private router: Router
  ) {
    this.classes = [];
    this.loading = true;
  }

  ngOnInit() {
    this.getClassesList(null);
  }


  doRefresh(event) {
    this.getClassesList(event);
  }

  // format the course reponse data
  formatClassesResponseData(data: any) {
    if (data && data.message === 'success') {
      this.classes = data.classes;
    }
  }

  // Get the course list
  getClassesList(event: any) {
    const currentUser = this.userService.getUserData();
    console.log(currentUser);
    this.classe.getSchoolClasses(currentUser.etablissement_scolaire_id).subscribe((reponse: any) => {
      this.formatClassesResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {

      this.loading = false;
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.loading = true;
          this.error.renewSession().then((data: any) => {
            if (data && data.result === 'OK') {
              this.getClassesList(event);
            } else {
              this.loading = false;
            }
          });

        } else {
          this.classError.manageClassError(error);
        }

      } else {
        this.error.manageSystemError(error);
      }

    });
  }

  // Get the attendance periode
  gotoAttendancePeriode(classId: number) {
    this.router.navigate(['teacher','attendance','attendance-periode',classId]);
  }

}
