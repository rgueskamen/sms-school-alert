import { UtilsService } from './../../shared/service/utils.service';
import { Router } from '@angular/router';
import { ErrorAdminSchoolService } from './../../school/service/error-admin-school.service';
import { EtabliServiceService } from './../../school/service/admin-school-service.service';
import { ErrorClassService } from './../../school/service/error-class.service';
import { ClasseServiceService } from './../../school/service/classe-service.service';
import { UserService } from './../../user/service/user-service.service';
import { UiService } from './../../shared/service/ui.service';
import { DateserviceService } from 'src/app/shared/service/dateservice.service';
import { ErrorSystemService } from './../../shared/service/error-system.service';
import { ErrorAttendanceService } from './../service/error-attendance.service';
import { AttendanceService } from './../../student/service/attendance.service';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-check-attendance',
  templateUrl: './check-attendance.page.html',
  styleUrls: ['./check-attendance.page.scss'],
})
export class CheckAttendancePage implements OnInit {

  classId: number;
  schoolId: any;
  validationMessages: any;
  dataForm: FormGroup;
  loading: boolean;
  monthsList: any;
  studentList: any;
  studentSchool: any;
  attendancePeriode: any;
  classes: any;
  hourList: any;
  constructor(
    private translate: TranslateService,
    private router: Router,
    private admin: EtabliServiceService,
    private ngZone: NgZone,
    private ErrorSchool: ErrorAdminSchoolService,
    private fb : FormBuilder,
    private attendance: AttendanceService,
    private errorAttendance: ErrorAttendanceService,
    private system: ErrorSystemService,
    private date: DateserviceService,
    private util: UtilsService,
    private ui: UiService,
    private userService: UserService,
    private classe: ClasseServiceService,
    private classError: ErrorClassService
  ) {
      this.loading = false;
      this.monthsList = this.date.getMonths();
      this.classes = [];
      this.hourList = this.setHours();
      this.attendancePeriode = [];
      this.studentList = [];
  }

  ngOnInit() {
    this.initFormData();
    this.validationMessage();
    this.getClassesList();
  }

  // set hours
  setHours() {
    const hourList = [];
    for(let i = 1; i <= 9; i++) {
      hourList.push(i);
    }
    return hourList;
  }

  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        fiche_presence_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        date_du_jour: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        numero_heure: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        ,
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }

  // Form getters
  get fiche_presence_id() {
    return this.dataForm.get('fiche_presence_id');
  }

  get date_du_jour() {
    return this.dataForm.get('date_du_jour');
  }

  get numero_heure() {
    return this.dataForm.get('numero_heure');
  }

  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  // Init the form
  initFormData() {
    const currentDate = new Date ();
    this.dataForm = this.fb.group({
      classe_id:['', Validators.required],
      fiche_presence_id: ['', Validators.required],
      date_du_jour: [this.date.formatDateTiret(currentDate), Validators.required],
      numero_heure: ['1', Validators.required],
      liste_eleve: [[], Validators.required]
    });
  }

  // update format student list
  updateStudentList() {
    const students = [];
    if (this.studentList && this.studentList.length > 0) {
      this.studentList.forEach(student => {
        students.push({user_id: student.eleve.id, is_present: student.select ? 1 : 0 });
    });
    }
 
    this.dataForm.get('liste_eleve').setValue(students);
  }

    // format the course reponse data
    formatClassesResponseData(data: any) {

   
      if (data && data.message === 'success') {

        this.ngZone.run(() => {
          this.classes = data.classes;
        });

        setTimeout(() => {
          if(this.classes && this.classes.length > 0) {
            if (this.classes[0] && this.classes[0].classe_id) {
              this.getStudentList(this.classes[0].classe_id);
              this.getAttendancePeriodeList(this.classes[0].classe_id);
              this.dataForm.get('classe_id').setValue(this.classes[0].classe_id);
            }
          }
        }, 200);

      }
    }

    // update data 
    updateData(classId: any) {
      if (classId) {
        this.getStudentList(classId);
        this.getAttendancePeriodeList(classId);
      }
    }

    // Get the course list
    getClassesList() {
      const currentUser = this.userService.getUserData();
      this.classe.getSchoolClasses(currentUser.etablissement_scolaire_id).subscribe((reponse: any) => {
        this.formatClassesResponseData(reponse);
        this.loading = false;
      }, error => {

        this.loading = false;

        if (error && error.error && error.error.message === 'error') {

          if (error && error.error && error.error.user_not_found) {
            this.loading = true;
            this.system.renewSession().then((data: any) => {
              if (data && data.result === 'OK') {
                this.getClassesList();
              } else {
                this.loading = false;
              }
            });

          } else {
            this.classError.manageClassError(error);
          }

        } else {
          this.system.manageSystemError(error);
        }

      });
    }

  // Get the list of student
  getStudentList(classId: any) {
    this.admin.getStudentWithParent(classId).subscribe((reponse: any) => {
      console.log(reponse);
      if (reponse && reponse.message === 'success') {
        this.studentList = reponse.eleves;

        console.log(this.studentList);

        this.studentList.forEach(data => {
          if (data) {
            data.select = true;
          }
        });

        console.log(this.studentList);

        this.studentSchool = reponse.etablissement_scolaire;
        this.updateStudentList();
      }
    }, error => {

      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.system.renewSession().then((data: any) => {
            if (data && data.result === 'OK') {
              this.getStudentList(classId);
            }
          });

        } else {
          this.ErrorSchool.manageAdminSchoolError(error);
        }

      } else {
        this.system.manageSystemError(error);
      }

    });
  }
      // Format the attendance response
      formatAttendanceResponseData(reponse: any) {
        if (reponse && reponse.message === 'success') {
          this.attendancePeriode = this.util.orderPropertyByKeyDown(reponse.liste_fiche,'fiche_presence','active');
        }
      }
      // Get the attendance periode
      getAttendancePeriodeList(classId) {
        const currentUser = this.userService.getUserData();
        this.attendance.getAllAttendance(currentUser.etablissement_scolaire_id,classId).subscribe((reponse: any) => {
          this.formatAttendanceResponseData(reponse);

        }, error => {

          if (error && error.error && error.error.message === 'error') {
            if (error && error.error && error.error.user_not_found) {
              this.system.renewSession().then((data: any) => {
                if (data && data.result === 'OK') {
                  this.getAttendancePeriodeList(classId);
                }
              });
            } else {
              this.errorAttendance.manageAttendanceError(error);
            }
          } else {
            this.system.manageSystemError(error);
          }
        });
      }

  // save attendance student
  checkStudentAttendance() {
    this.loading = true;
    const param = this.dataForm.value;
    param.date_du_jour = this.date.formatDate(param.date_du_jour);
    this.attendance.saveAttendanceByHour(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('STUDENT_ATTENDANCE_SAVE_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.router.navigate(['teacher','attendance']);
        }
      }, error => {
        if (error && error.error && error.error.message === 'error') {
          if (error.error.user_not_found) {
            this.system.renewSession().then((data:any) => {
                  if (data && data.result === 'OK') {
                      this.checkStudentAttendance();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.errorAttendance.manageAttendanceError(error);
          }
        } else {
          this.loading = false;
          this.system.manageSystemError(error);
        }
    });
  }

}
