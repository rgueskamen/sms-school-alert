import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckAttendancePageRoutingModule } from './check-attendance-routing.module';

import { CheckAttendancePage } from './check-attendance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    CheckAttendancePageRoutingModule
  ],
  declarations: [CheckAttendancePage]
})
export class CheckAttendancePageModule {}
