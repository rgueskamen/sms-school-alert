import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CheckAttendancePage } from './check-attendance.page';

describe('CheckAttendancePage', () => {
  let component: CheckAttendancePage;
  let fixture: ComponentFixture<CheckAttendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckAttendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CheckAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
