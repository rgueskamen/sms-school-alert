import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';
import { TeacherMenuComponent } from './teacher-menu/teacher-menu.component';


@Component({
  selector: 'app-teacher-dash',
  templateUrl: 'teacher-dash.page.html',
  styleUrls: ['teacher-dash.page.scss'],
})
export class TeacherDashPage implements OnInit {
  haveInvitation: boolean;
  nbInvitations: number;
  backService: any;

  constructor(
    private popoverController: PopoverController
  ) {

  }

  ngOnInit() {
  }

  // Open contextual menu
  async openContextMenu(ev: any) {
    const popover = await this.popoverController.create({
      component: TeacherMenuComponent,
      event: ev
    });
    return await popover.present();
  }

}
