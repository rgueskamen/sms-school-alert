import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SyllabusCoursePageRoutingModule } from './syllabus-course-routing.module';

import { IonicModule } from '@ionic/angular';
import { SyllabusCoursePage } from './syllabus-course.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    SyllabusCoursePageRoutingModule,
    SharedModule
  ],
  declarations:[
    SyllabusCoursePage
  ]
})
export class SyllabusCoursePageModule { }
