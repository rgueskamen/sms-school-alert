import { ErrorSyllabusService } from './../../../teacher/service/error-syllabus.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { SyllabusService } from './../../../parent/service/syllabus.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { UserService } from 'src/app/user/service/user-service.service';
import { CoursService } from '../../service/cours.service';

@Component({
  selector: 'app-syllabus-detail',
  templateUrl: 'syllabus-detail.page.html',
  styleUrls: ['syllabus-detail.page.scss']
})
export class SyllabusDetailPage  implements OnInit{
  chapters: any[] = [];
  loading: boolean;
  courseID: any;
  classID: any;
  currentUser: any;

  constructor(
    private userService: UserService,
    private course: CoursService,
    private activeRoute: ActivatedRoute,
    private event: EventsService,
    private syllabus: SyllabusService,
    private error: ErrorSystemService,
    private syllabusError: ErrorSyllabusService,
    private utils: UtilsService
  ) {
    this.loading = false;
    this.courseID = this.activeRoute.snapshot.params.courseId;
    this.currentUser = this.userService.getUserData();
    this.classID = this.currentUser.classe_id;
    console.log(this.classID);
  }

  ngOnInit(): void {
    this.getAllChapter(this.currentUser.classe_id,this.courseID);
  }

  doRefresh(event) {
    this.getAllChapter(this.currentUser.classe_id,this.courseID,event);
  }

  // get the list of chapter
  getAllChapter(classId: any, courseId: any, event ?:any) {
    this.syllabus.getChapter(classId, courseId)
      .subscribe((reponse: any) => {
        if (reponse && reponse.message === 'success') {
          this.chapters = this.utils.orderByKeyDown(reponse.chapitres, 'numero');
        }
        if (event) {
          event.target.complete();
        }
      }, error => {
        if (event) {
          event.target.complete();
        }
        if (error && error.error && error.error.message === 'error') {
          if (error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === 'OK') {
                this.getAllChapter(classId, courseId, event);
              }
            });
          } else {
            this.syllabusError.manageSyllabusError(error);
          }
        } else {
          this.error.manageSystemError(error);
        }
      });
  }


}
