import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { SyllabusDetailPage } from './syllabus-detail.page';
import { SyllabusDetailPageRoutingModule } from './syllabus-detail-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    SyllabusDetailPageRoutingModule,
    SharedModule
  ],
  declarations: [
    SyllabusDetailPage
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SyllabusDetailPageModule { }
