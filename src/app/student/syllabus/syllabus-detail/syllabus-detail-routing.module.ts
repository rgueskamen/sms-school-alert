import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SyllabusDetailPage } from './syllabus-detail.page';

const routes: Routes = [
  {
    path: '',
    component: SyllabusDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SyllabusDetailPageRoutingModule { }
