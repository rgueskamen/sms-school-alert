import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ClasseServiceService } from 'src/app/school/service/classe-service.service';
import { ErrorClassService } from 'src/app/school/service/error-class.service';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';

@Component({
  selector: 'app-syllabus-course',
  templateUrl: 'syllabus-course.page.html',
  styleUrls: ['syllabus-course.page.scss']
})
export class SyllabusCoursePage implements OnInit {
  courses: any[] = [];
  classeId: number;
  loading: boolean;

  constructor(
    private classe: ClasseServiceService,
    private classError: ErrorClassService,
    private error: ErrorSystemService,
    private popoverController: PopoverController,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) {
      this.loading = true;
      this.classeId = this.activeRoute.snapshot.params.classId;
  }

  ngOnInit(): void {
    this.getCoursesList(null);
  }

  doRefresh(event) {
    this.getCoursesList(event);
  }

  // format the course reponse data
  formatCourseResponseData(data: any) {
      if (data && data.message === 'success') {
        this.courses = data.matieres;
      }
  }

  // Get the course list
  getCoursesList(event: any) {
    this.classe.getCoursesOfSchoolClasses(this.classeId).subscribe((reponse: any) => {
      this.formatCourseResponseData(reponse);
      this.loading = false;
      if (event) {
          setTimeout(() => {
            event.target.complete();
          },200);
      }
    }, error => {

      this.loading = false;
      if (event) {
          event.target.complete();
       }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.loading = true;
          this.error.renewSession().then((data: any) => {
                if (data && data.result === 'OK') {
                  this.getCoursesList(event);
                } else {
                  this.loading = false;
                }
          });
        } else {
          this.classError.manageClassError(error);
        }

      } else {
          this.error.manageSystemError(error);
      }

    });
  }

  // open the course syllabus
   viewDetail(courseData: any) {
      this.router.navigate(['student','syllabus',this.classeId,'syllabus-detail',courseData.id]);
  }

}
