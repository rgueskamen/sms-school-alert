import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SyllabusCoursePage } from './syllabus-course.page';

const routes: Routes = [
  {
    path: '',
    component: SyllabusCoursePage
  },
  {
    path: 'syllabus-detail/:courseId',
    loadChildren: () => import('./syllabus-detail/syllabus-detail.module').then(m => m.SyllabusDetailPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SyllabusCoursePageRoutingModule { }
