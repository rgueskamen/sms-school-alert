import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UiService } from 'src/app/shared/service/ui.service';


@Injectable({
  providedIn: 'root'
})
export class ErrorCoursService {

  constructor(
    private translate: TranslateService,
    private ui: UiService
  ) { }

  // Manage courses error
  manageCoursesError(error) {

    if (error && error.error && error.error.remplir_tous_les_champs) {
      this.translate.get('FILL_ALL_PARAMETERS_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.classe_id_not_exist) {
      this.translate.get('CLASS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.matiere_id_not_exist) {
      this.translate.get('COURSE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.professeur_id_not_exist) {
      this.translate.get('TEACHER_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.chapitre_id_not_exist) {
      this.translate.get('CHAPTER_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.sous_chapitre_id_not_exist) {
      this.translate.get('SUB_CHAPTER_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.sous_titre_chapitre_id_not_exist) {
      this.translate.get('SUB_TITLE_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    
    if (error && error.error && error.error.etablissement_scolaire_id_not_exist) {
      this.translate.get('SCHOOL_NOT_EXIST_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.this_professeur_not_exist_in_this_classe) {
      this.translate.get('TEACHER_NOT_EXIST_IN_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.cahier_texte_id_not_exist) {
      this.translate.get('TEXTBOOK_NOT_EXIST_IN_CLASS').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.eleve_id_not_exist) {
      this.translate.get('STUDENT_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.eleve_id_not_exist) {
      this.translate.get('STUDENT_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

    if (error && error.error && error.error.avis_eleve_id_not_exist) {
      this.translate.get('STUDENT_AVIS_NOT_EXIST_TEXT').subscribe(value => {
        this.ui.presentToast(value);
      });
    }

  }

}
