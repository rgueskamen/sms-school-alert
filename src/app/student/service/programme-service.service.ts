import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class ProgrammeService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }


  // Save test planning of a class
  saveTestPlanning(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`programme/evaluation/add/${token}`, data);
  }

  // Get active test planning  of a class
  getActivePlanning(classId: string) {
    const token = this.userService.getToken();
    return this.api.get(`programme/evaluation/get/programme/${classId}/${token}`);
  }

  // Update test planning of a class
  updateTestPlanning(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`programme/evaluation/edit/${token}`, data);
  }

  // Activate or disable test planning 
  activateOrDisableTestPlanning(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`programme/evaluation/activate/or/desactivate/${token}`, data);
  }

  // get the programm test of a class
  getClassProgram(classId: number) {
    const token = this.userService.getToken();
    return this.api.get(`programme/evaluation/get/all/programme/${classId}/${token}`);
  }

  // save a programm test date
  saveProgrammTestDate(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`programme/evaluation/save/date/depot/sujet/examen/${token}`, data);
  }

  // Get test programm date
  getProgrammTestDate(classId: number, courseId: number) {
    const token = this.userService.getToken();
    return this.api.get(`programme/evaluation/get/date/depot/sujet/examen/${classId}/${courseId}/${token}`);
  }

  // Update the programm test date
  updateProgramTestDate() {
    const token = this.userService.getToken();
    return this.api.get(`programme/evaluation/update/date/depot/sujet/examen/${token}`);
  }

  // delete a programme service
  deleteProgramme() {
    const token = this.userService.getToken();
    return this.api.get(`programme/evaluation/delete/${token}`);
  }


  // delete a programme service
  deleteTestDepositDate() {
      const token = this.userService.getToken();
      return this.api.get(`programme/evaluation/delete/date/depot/sujet/examen/${token}`);
  }


}
