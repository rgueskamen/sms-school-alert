import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { TranslateService } from '@ngx-translate/core';
import { PluginService } from 'src/app/shared/service/plugin.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { ProgrammeService } from 'src/app/teacher/service/programme-service.service';
import { ErrorProgrammeService } from 'src/app/teacher/service/error-programme.service';
import { ClasseServiceService } from 'src/app/school/service/classe-service.service';
import { ErrorClassService } from 'src/app/school/service/error-class.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Component({
  selector: 'app-program',
  templateUrl: './program.page.html',
  styleUrls: ['./program.page.scss'],
})
export class ProgramPage implements OnInit {

  classes: any;
  loading: boolean;
  classId: any;
  activeProgram: any;

  constructor(
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private classe: ClasseServiceService,
    private activateRoute: ActivatedRoute,
    private classError: ErrorClassService,
    private userService: UserService,
    private error: ErrorSystemService,
    private plugin: PluginService,
    private translate: TranslateService,
    private ui: UiService
  ) {
    this.loading = false;
    this.classId = this.activateRoute.snapshot.params.classId;
  }

  ngOnInit() {
    this.getClassesList();
    this.getActiveProgram(null);
  }

  doRefresh(event) {
    this.getClassesList();
    this.getActiveProgram(event);
  }

  getActiveProgram(event:any) {
    this.program.getActivePlanning(this.classId).subscribe((reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.activeProgram = reponse.programme;
        }

        if (event) {
          setTimeout(() => {
              event.target.complete();
          },200);
        }
    }, error => {

      if (event) {
        event.target.complete();
      }

      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getActiveProgram(event);
            }
          });
        } else {
          this.programError.managePlanningError(error);
        }
      } else {
        this.error.manageSystemError(error);
      }
    });
  }

  // format the course reponse data
  formatClassesResponseData(data: any) {
    if (data && data.message === "success") {

      if (data.classes && data.classes.length > 0) {
        const currentUser = this.userService.getUserData();
        this.classes = data.classes.filter(data => { return data.classe_id ===  currentUser.classe_id });
      }

    }
  }

  // Get the course list
  getClassesList() {
    const currentUser = this.userService.getUserData();
    this.classe.getSchoolClasses(currentUser.etablissement_scolaire_id).subscribe((reponse: any) => {
      this.formatClassesResponseData(reponse);
    }, error => {
      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getClassesList();
            }
          });
        } else {
          this.classError.manageClassError(error);
        }
      } else {
        this.error.manageSystemError(error);
      }
    });
  }

  // view program
  viewProgram(data: any) {
    if (data.programme) {
      this.plugin.showFileServer(data.programme);
    } else {
      this.translate.get('NONE_DOCUMENT').subscribe(trans => {
        this.ui.presentToast(trans);
      });
    }
  }

}
