import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentDashPage } from './student-dash.page';

const routes: Routes = [
    {
        path: '',
        component: StudentDashPage
    },
    { path: 'program/:classId', loadChildren: () => import('./program/program.module').then(m => m.ProgramPageModule) },
    { path: 'course/:classId', loadChildren: () => import('./course/course.module').then(m => m.CoursePageModule) },
    { path: 'syllabus/:classId', loadChildren: () => import('./syllabus/syllabus-course.module').then(m => m.SyllabusCoursePageModule) }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class StudentDashRoutingPageModule {

}
