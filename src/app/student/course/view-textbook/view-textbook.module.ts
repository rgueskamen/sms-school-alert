import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { ViewTextBookPage } from './view-textbook.page';
import { ViewTeextBookPageRoutingModule } from './view-textbook-routing.module';
import { AdviseMenuComponent } from './student-advise/advise-menu/advice-menu.component';
import { EditAdvisePage } from './student-advise/edit-advise/edit-advise.page';
import { DeleteAdviseComponent } from './student-advise/delete-advise/delete-advise.component';
import { StudentAdvisesPage } from './student-advise/student-advise.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ViewTeextBookPageRoutingModule,
    SharedModule
  ],
  declarations: [
    ViewTextBookPage,
    DeleteAdviseComponent,
    StudentAdvisesPage,
    EditAdvisePage
  ],
  entryComponents: [
    AdviseMenuComponent,
    DeleteAdviseComponent,
    StudentAdvisesPage,
    EditAdvisePage
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewTextBookPageModule { }
