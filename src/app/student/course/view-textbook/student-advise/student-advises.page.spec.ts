import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAdvisesPage } from './student-advise.page';

describe('ExplorePage', () => {
  let component: StudentAdvisesPage;
  let fixture: ComponentFixture<StudentAdvisesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StudentAdvisesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
