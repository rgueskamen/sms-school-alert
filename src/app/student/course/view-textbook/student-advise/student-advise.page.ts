import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { CoursService } from '../../../service/cours.service';
import { ErrorCoursService } from '../../../service/error-cours.service';
import { EtabliServiceService } from 'src/app/school/service/admin-school-service.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { TranslateService } from '@ngx-translate/core';
import { AdviseMenuComponent } from './advise-menu/advice-menu.component';
import { EventsService } from 'src/app/shared/service/events.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Component({
  selector: 'app-student-advises',
  templateUrl: 'student-advises.page.html',
  styleUrls: ['student-advises.page.scss']
})
export class StudentAdvisesPage implements OnInit {

  studentAdvise: any;
  textBook: any;
  loading: boolean;
  classId: any;
  studentList: any;
  studentSchool: any;
  advise: string;
  userId: any;

  constructor(
    private course: CoursService,
    private popoverController: PopoverController,
    private userService: UserService,
    private courseError: ErrorCoursService,
    private admin: EtabliServiceService,
    private navparam: NavParams,
    private modatCtrl: ModalController,
    private util: UtilsService,
    private error: ErrorSystemService,
    private events: EventsService,
    private ui: UiService,
    private translate: TranslateService
  ) {
    this.studentAdvise = [];
    this.loading = true;
    this.textBook = this.navparam.get('textbook');
    this.classId = this.navparam.get('classId');
    this.studentList = [];
    this.advise = '';
    this.userId = this.navparam.get('userId');
    this.events.subscribe('advise-update', () => {
      this.getStudentAdviseList(null);
    });
  }


  ngOnInit(): void {
    this.getStudentAdviseList(null);
    this.getStudentList();
  }

  doRefresh(event) {
    this.getStudentAdviseList(event);
  }

  // close the modal
  closeModal() {
    this.modatCtrl.dismiss();
  }


  // format student qdvise reponse data
  formatStudentAdviseResponseData(data: any) {
    if (data && data.message === 'success') {
      this.studentAdvise = this.util.orderByKeyUp(data.avis_eleve, 'updated_at');
    }
  }

  // Get the student advise
  getStudentAdviseList(event: any) {

    this.course.getStudentAdvise(this.textBook.cahier_texte_id).subscribe((reponse: any) => {
      this.formatStudentAdviseResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {

      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === 'OK') {
              this.getStudentAdviseList(event);
            } else {
              this.loading = false;
            }
          });

        } else {
          this.loading = false;
          this.courseError.manageCoursesError(error);
        }

      } else {
        this.loading = false;
        this.error.manageSystemError(error);
      }

    });
  }

  // Get the list of student
  getStudentList() {
    this.admin.getStudentWithParent(this.classId).subscribe((reponse: any) => {
      console.log(reponse);
      if (reponse && reponse.message === 'success') {
        this.studentList = reponse.eleves;
        this.studentSchool = reponse.etablissement_scolaire;
      }
    }, error => {

      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === 'OK') {
              this.getStudentList();
            }
          });

        } else {
          this.courseError.manageCoursesError(error);
        }

      } else {
        this.error.manageSystemError(error);
      }

    });
  }


  // get the  current student data
  getCurrentStudentData(studentList: any[], studentId: number) {
    if (studentList && studentList.length > 0) {
      return studentList.find((value, index, student) => { return student[index].eleve.id === studentId });
    } else return [];
  }


  // send the user comment
  sendComment() {

    this.translate.get('SENDING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });

    const param = {
      cahier_texte_id: this.textBook.cahier_texte_id,
      eleve_id: this.userId,
      avis_eleve: this.advise
    };

    this.course.saveStudentAdvise(param).subscribe((reponse: any) => {

      if (reponse && reponse.message === 'success') {
        this.getStudentAdviseList(null);
      }
      this.ui.dismissLoading();

    }, error => {

      if (error && error.error && error.error.message === 'error') {

        if (error.error.user_not_found) {

          this.error.renewSession().then((data: any) => {
            this.ui.dismissLoading();
            if (data && data.result === 'OK') {
              this.sendComment();
            }
          });

        } else {
          this.ui.dismissLoading();
          this.courseError.manageCoursesError(error);
        }
      } else {
        this.ui.dismissLoading();
        this.error.manageSystemError(error);
      }

    });

  }


  async showOption(advise: any) {

    const currentUser = this.userService.getUserData();

    if (advise && advise.eleve_id === currentUser.id) {
      const popover = await this.popoverController.create({
        component: AdviseMenuComponent,
        componentProps: {
          data: advise
        }
      });
      return await popover.present();
    }
  }

}
