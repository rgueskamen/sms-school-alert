import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavParams, PopoverController } from '@ionic/angular';
import { EventsService } from 'src/app/shared/service/events.service';
import { DeleteAdviseComponent } from '../delete-advise/delete-advise.component';
import { EditAdvisePage } from '../edit-advise/edit-advise.page';

@Component({
  selector: 'app-advice-menu',
  templateUrl: './advice-menu.component.html',
  styleUrls: ['./advice-menu.component.scss'],
})
export class AdviseMenuComponent implements OnInit {
  data: any;
  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private event: EventsService,
    private modatCtrl: ModalController
  ) {
    this.data = this.navParams.get('data');
  }

  ngOnInit() { }

  closeMenu() {
    this.popoverController.dismiss();
  }


  // Open the modal
  editAdvise() {
    this.closeMenu();
    this.modatCtrl
      .create({
        component: EditAdvisePage,
        componentProps: {
          data:  this.data
        }
      })
      .then(modalEl => {
        modalEl.present();
        modalEl.onDidDismiss().then((ans) => {
          if (ans && ans.data === 'updated') {
            this.event.publish('advise-update');
           }
        });
      });
  }

    // Delete the swap request
    async delteAdvise() {
      this.closeMenu();
      const popover = await this.popoverController.create({
        component: DeleteAdviseComponent,
        cssClass: 'delete-popover',
        componentProps: {
          data:  this.data
        }
      });

      popover.onDidDismiss().then((ans) => {
        if (ans && ans.data === 'deleted') {
          this.event.publish('advise-update');
         }
      });
      return await popover.present();
    }

}
