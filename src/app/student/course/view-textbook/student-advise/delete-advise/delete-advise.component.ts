import { Component, OnInit } from '@angular/core';
import { NavParams, PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { CoursService } from '../../../../service/cours.service';
import { ErrorCoursService } from '../../../../service/error-cours.service';

@Component({
  selector: 'app-delete-advise',
  templateUrl: './delete-advise.component.html',
  styleUrls: ['./delete-advise.component.scss'],
})
export class DeleteAdviseComponent implements OnInit {

  data: any;

  constructor(
    public popoverController: PopoverController,
    private event: EventsService,
    private course: CoursService,
    private courseError: ErrorCoursService,
    private translate: TranslateService,
    private ui: UiService,
    private errorService: ErrorSystemService,
    private navParams: NavParams
  ) {
      this.data = this.navParams.get('data');
  }

  ngOnInit() {}

  close(ans ?: any) {
    this.popoverController.dismiss(ans, null);
  }

  // Delete a comment
  deleteComment() {
    const data = { avis_eleve_id : this.data.id};
    this.translate.get('DELETE_LOADING_TEXT').subscribe(trans => {
      this.ui.presentLoading(trans);
    });
    this.course.deleteStudentAdvise(data)
      .subscribe((reponse: any) => {
        this.ui.dismissLoading();
        if (reponse && reponse.message === 'success') {
            this.close('deleted');
            this.event.publish('advise-update');
            this.translate.get('ADVISE_DELETE_TEXT').subscribe(trans => {
              this.ui.presentToast(trans);
            });
        }
      }, error => {
        if (error && error.error && error.error.message === 'error') {
          if (error && error.error && error.error.user_not_found) {
              this.errorService.renewSession().then((ans: any) => {
                    this.ui.dismissLoading();
                    if (ans && ans.result === 'OK') {
                        this.deleteComment();
                    } else {
                      this.close();
                    }
              });
          } else {
            this.close();
            this.ui.dismissLoading();
            this.courseError.manageCoursesError(error);
          }
        } else {
          this.close();
          this.ui.dismissLoading();
            this.errorService.manageSystemError(error);
        }
      });
  }

}
