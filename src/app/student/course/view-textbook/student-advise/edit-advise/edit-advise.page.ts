import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { CoursService } from '../../../../service/cours.service';
import { ErrorCoursService } from '../../../../service/error-cours.service';

@Component({
  selector: 'app-edit-advise',
  templateUrl: './edit-advise.page.html',
  styleUrls: ['./edit-advise.page.scss'],
})
export class EditAdvisePage implements OnInit {
  loading: boolean;
  dataForm: FormGroup;
  validationMessages: any;
  data: any;

  constructor(
    private fb: FormBuilder,
    private location: TranslateService,
    private course: CoursService,
    private courseError: ErrorCoursService,
    private navParams: NavParams,
    private errorService: ErrorSystemService,
    private event: EventsService,
    private modatCtrl: ModalController
  ) {
    this.loading = false;
    this.data = this.navParams.get('data');
  }

  ngOnInit() {
    this.validationMessage();
    this.initUserForm();
  }

  get advise() {
    return this.dataForm.get('avis_eleve');
  }

  // init the user form with his data
  initUserForm() {
    this.dataForm = this.fb.group({
      avis_eleve_id:[this.data ? this.data.avis : '', Validators.required],
      cahier_texte_id: [this.data ? this.data.avis : '', Validators.required],
      eleve_id: [this.data ? this.data.avis : '', Validators.required],
      avis_eleve: [this.data ? this.data.avis : '', Validators.required]
    });
  }

  // Validation messagvalidationMessagese
  validationMessage() {
    this.location.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        advise: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }

  // close the modal
  closeModal(ans?:any) {
    this.modatCtrl.dismiss(ans,null);
  }

  // update student comment
  updateComment() {
    this.loading = true;
    this.course.updateStudentAdvise(this.dataForm.value).subscribe(
      (reponse: any) => {
        if (reponse && reponse.message === 'success') {
          this.event.publish('advise-update');
        }
        this.loading = false;
        this.closeModal('updated');
      }, error => {
        if (error && error.error) {
          if (error && error.error && error.error.user_not_found) {
            this.errorService.renewSession().then((data: any) => {
              if (data && data.result === 'OK') {
                this.updateComment();
              } else {
                this.loading = false;
                this.closeModal();
              }
            });
          } else {
            this.loading = false;
            this.closeModal();
            this.courseError.manageCoursesError(error);
          }
        } else {
          this.loading = false;
          this.closeModal();
          this.errorService.manageSystemError(error);
        }
      });
  }

}
