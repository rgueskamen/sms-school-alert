import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursePage } from './course.page';

const routes: Routes = [
  {
    path: '',
    component: CoursePage
  },
  {
    path: 'textbook/view/:courseId',
    loadChildren: () => import('./view-textbook/view-textbook.module').then(m => m.ViewTextBookPageModule)
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsPageRoutingModule { }
