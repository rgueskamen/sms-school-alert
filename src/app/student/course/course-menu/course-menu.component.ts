import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-course-menu',
  templateUrl: './course-menu.component.html',
  styleUrls: ['./course-menu.component.scss'],
})
export class CourseMenuComponent implements OnInit {
  course: any;
  classeId: any;
  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private router: Router
  ) {
    this.course = this.navParams.get('course');
    this.classeId =  this.navParams.get('classId');
  }

  ngOnInit() { }

  closeCourseMenu() {
    this.popoverController.dismiss();
  }


  // View textbook
  viewTextBook() {
    this.closeCourseMenu();
  }


}
