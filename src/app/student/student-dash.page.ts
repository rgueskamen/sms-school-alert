import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';
import { UserService } from '../user/service/user-service.service';
import { StudentMenuComponent } from './student-menu/student-menu.component';


@Component({
  selector: 'app-student-dash',
  templateUrl: 'student-dash.page.html',
  styleUrls: ['student-dash.page.scss'],
})
export class StudentDashPage implements OnInit {
  currentUser:any;

  constructor(
    private popoverController: PopoverController,
    private userService: UserService
  ) {
      this.currentUser = this.userService.getUserData();
  }

  ngOnInit() {
  }

  // Open contextual menu
  async openContextMenu(ev: any) {
    const popover = await this.popoverController.create({
      component: StudentMenuComponent,
      event: ev
    });
    return await popover.present();
  }

}
