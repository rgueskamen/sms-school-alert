import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { StudentDashRoutingPageModule } from './student-dash-routing.module';
import { StudentDashPage } from './student-dash.page';
import { StudentMenuComponent } from './student-menu/student-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    StudentDashRoutingPageModule
  ],
  declarations: [
    StudentDashPage,
    StudentMenuComponent
  ],
  entryComponents: [
    StudentMenuComponent
  ]
})
export class StudentDashPageModule {}
