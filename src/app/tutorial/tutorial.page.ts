import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides, NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  keys: any;
  slideData: any;
  sliderLoop: any;

  @ViewChild(IonSlides,{static: false}) slides: IonSlides;

  constructor(
    private translate: TranslateService,
    private router: Router,
    private nav: NavController
  ) {

    this.keys = [];
    this.slideData = [];
  }

  // initialiwe the slider
  initailiseSlider() {
    this.keys = [
      'TUTORIAL_SLIDE1_TITLE',
      'TUTORIAL_SLIDE1_DESCRIPTION',
      'TUTORIAL_SLIDE2_TITLE',
      'TUTORIAL_SLIDE2_DESCRIPTION',
      'TUTORIAL_SLIDE3_TITLE',
      'TUTORIAL_SLIDE3_DESCRIPTION',
      'TUTORIAL_SLIDE4_TITLE',
      'TUTORIAL_SLIDE4_DESCRIPTION',
      'TUTORIAL_SLIDE5_TITLE',
      'TUTORIAL_SLIDE5_DESCRIPTION'
    ];

    this.translate.get(this.keys).subscribe((value) => {
      this.slideData = [
        {
          title: value.TUTORIAL_SLIDE1_TITLE,
          picture: 'assets/img/1.png',
          description: value.TUTORIAL_SLIDE1_DESCRIPTION
        },
        {
          title: value.TUTORIAL_SLIDE2_TITLE,
          picture: 'assets/img/2.png',
          description: value.TUTORIAL_SLIDE2_DESCRIPTION
        },
        {
          title: value.TUTORIAL_SLIDE3_TITLE,
          picture: 'assets/img/bg-03.png',
          description: value.TUTORIAL_SLIDE3_DESCRIPTION
        },
        {
          title: value.TUTORIAL_SLIDE4_TITLE,
          picture: 'assets/img/3.png',
          description: value.TUTORIAL_SLIDE4_DESCRIPTION
        },
        {
          title: value.TUTORIAL_SLIDE5_TITLE,
          picture: 'assets/img/4.png',
          description: value.TUTORIAL_SLIDE5_DESCRIPTION
        }];
    });

    this.sliderLoop =  setInterval(() => {
      this.slides.getActiveIndex().then(activeIndex => {
        if (activeIndex !== 4) {
          this.slides.slideTo(activeIndex + 1,300);
        }
      });
    },4000);
  }

  ngOnInit() {
    this.initailiseSlider();
  }

  // go to the login page
  gotoPageAuth() {
      this.nav.setDirection('root');
      this.router.navigate(['user/user-login']);
  }


  // auto swipe
  autoSlide() {
   this.slides.getActiveIndex().then(index => {
     if (index === 4) {

        setTimeout(() => {
          this.gotoPageAuth();
        }, 5000);
      }
   });
  }

  ionViewWillLeave() {
    clearInterval(this.sliderLoop);
  }

}
