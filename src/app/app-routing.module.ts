import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AppGuard } from './app.guard';

const routes: Routes = [

  {
    path: '', redirectTo:'home', pathMatch:'full'
},
  {
    path: 'teacher',
    loadChildren: () => import('./teacher/teacher-dash.module').then( m => m.TeacherDashPageModule)
  },
  {
    path: 'parent',
    loadChildren: () => import('./parent/parent-dash.module').then( m => m.ParentDashPageModule)
  },
  {
    path: 'student',
    loadChildren: () => import('./student/student-dash.module').then( m => m.StudentDashPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
     canLoad: [AppGuard]
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then( m => m.UserModule)
  },
 
  {
    path: 'tutorial',
    loadChildren: () => import('./tutorial/tutorial.module').then( m => m.TutorialPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
