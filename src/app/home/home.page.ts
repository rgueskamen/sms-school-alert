import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { TermesComponent } from './termes/termes.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
    private router: Router,
    private nav: NavController,
    public modal: ModalController
  ) {}


  // Go to the tutorial
  nextButton() {
      this.nav.setDirection('root');
      this.router.navigate(['tutorial']);
  }


  // open termes and conditions
 async openTerme(ev: any) {
  const  modal = await this.modal.create({
      component: TermesComponent
    });
    return await modal.present();
  }

}
