import { Component, ViewChild } from '@angular/core';
import { ModalController, NavController, NavParams} from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the TermesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */


@Component({
    selector: 'app-termes',
    templateUrl: 'termes.component.html',
    styleUrls: ['termes.component.scss'],
})
export class TermesComponent {

    language:string;
    showNextBtn:boolean;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modal: ModalController,
        public translate:TranslateService,
    ) {
        this.language=  translate.getBrowserLang() || "en";
        this.showNextBtn = false;
    }


    close(){
        this.modal.dismiss();
    }

    getScrolling(ev) {
        if (ev && ev.detail && ev.detail.scrollTop > 3000) {
            this.showNextBtn = true;
        }
    }


}
