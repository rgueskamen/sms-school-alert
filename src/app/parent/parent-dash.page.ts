import { Component, OnInit } from '@angular/core';
import { PopoverController} from '@ionic/angular';
import { ParentMenuComponent } from './parent-menu/parent-menu.component';


@Component({
  selector: 'app-parent-dash',
  templateUrl: 'parent-dash.page.html',
  styleUrls: ['parent-dash.page.scss'],
})
export class ParentDashPage implements OnInit {
  haveInvitation: boolean;
  nbInvitations: number;
  backService: any;

  constructor(
    private popoverController: PopoverController
  ) {

  }

  ngOnInit() {
  }

  // Open contextual menu
  async openContextMenu(ev: any) {
    const popover = await this.popoverController.create({
      component: ParentMenuComponent,
      event: ev
    });
    return await popover.present();
  }

}
