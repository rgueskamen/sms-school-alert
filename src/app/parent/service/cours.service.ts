import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class CoursService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }


  // Fill test book
  fillTestBook(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/add/${token}`, data);
  }

  
  // Get text book information
  getClassTextBookInfo(schoolId: string, classId: string, courseId: string) {
    const token = this.userService.getToken();
    return this.api.get(`session/cours/get/${schoolId}/${classId}/${courseId}/${token}`);
  }


  // Update test book information
  updateTestBookInfo(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/edit/${token}`, data);
  }


  // Delete text book information
  deleteTextBookInfo(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/delete/${token}`, data);
  }


  // Save student advise
  saveStudentAdvise(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/save/avis/eleve/${token}`, data);
  }


  // Get student advise on a course
  getStudentCourseAdvise(studentId: string, textBookId: string) {
    const token = this.userService.getToken();
    return this.api.get(`session/cours/get/avis/eleve/${studentId}/${textBookId}/${token}`);
  }


  // get all student advise on a course
  getStudentAdvise(textBookId: string) {
    const token = this.userService.getToken();
    return this.api.get(`session/cours/get/avis/all/eleves/${textBookId}/${token}`);
  }


  // update student advise
  updateStudentAdvise(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/edit/avis/eleve/${token}`, data);
  }


  // delete student advise
  deleteStudentAdvise(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`session/cours/delete/avis/eleve/${token}`, data);
  }

}
