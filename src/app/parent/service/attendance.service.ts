import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }


  // Save a attendance file
  saveAttendance(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`fiche/presence/save/fiche/${token}`, data);
  }

  
  // Get all attendance
  getAllAttendance(schoolId: string, classId: string) {
    const token = this.userService.getToken();
    return this.api.get(`fiche/presence/get/fiche/${schoolId}/${classId}/${token}`);
  }


  // Enable or disable attendance
  enableOrDisableAttendance(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`fiche/presence/activate/or/desactivate/fiche/${token}`, data);
  }


  // Save attendance by hour
  saveAttendanceByHour(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`fiche/presence/save/presence/or/absence/student/${token}`, data);
  }


  // save attendance advise
  saveAttendanceAdvise(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`fiche/presence/save/observation/${token}`, data);
  }


  // Get attendance information
  getAttendanceInfo(attendanceId: number) {
    const token = this.userService.getToken();
    return this.api.get(`fiche/presence/get/detail/fiche/${attendanceId}/${token}`);
  }


  // Update attendance
  updateAttendance() {
    const token = this.userService.getToken();
    return this.api.get(`fiche/presence/update/presence/or/absence/student/${token}`);
  }

}
