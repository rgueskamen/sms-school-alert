import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/service/api.service';
import { UserService } from 'src/app/user/service/user-service.service';

@Injectable({
  providedIn: 'root'
})
export class SyllabusService {

  constructor(
    private api: ApiService,
    private userService: UserService
  ) { }


  //save a chapter
  saveChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/add/chapitre/${token}`, data);
  }


  // Get chapter
  getChapter(classId: string, courseId: string) {
    const token = this.userService.getToken();
    return this.api.get(`syllabus/get/chapitre/${classId}/${courseId}/${token}`);
  }


  // Update chapter
  updateChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/edit/chapitre/${token}`, data);
  }


  // Delete chapter
  deleteChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/delete/chapitre/${token}`, data);
  }


  // Save subChapter
  saveSubChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/add/sous/chapitre/${token}`, data);
  }


  // Get subChapter
  getSubChapter(chapterId: string, classId: string, courseId: string) {
    const token = this.userService.getToken();
    return this.api.get(`syllabus/get/sous/chapitre/${chapterId}/${classId}/${courseId}/${token}`);
  }


  // Update a subchapter
  updateSubChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/edit/sous/chapitre/${token}`, data);
  }


  // delete sub chapter
  deleteSubChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/delete/sous/chapitre/${token}`, data);
  }


  //add a subtitle
  addSubTitle(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/add/sous/titre/chapitre/${token}`, data);
  }

  // Get subTitle
  getSubTitleSubChapter(subChapterId: string, classId: string, courseId: string) {
    const token = this.userService.getToken();
    return this.api.get(`syllabus/get/sous/titre/chapitre/${subChapterId}/${classId}/${courseId}/${token}`);
  }

  // Update subchapter
  updateSubTitleSubChapter(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/edit/sous/titre/chapitre/${token}`, data);
  }

  // delete subTitle
  deleteSubTitle(data: any) {
    const token = this.userService.getToken();
    return this.api.post(`syllabus/delete/sous/titre/chapitre/${token}`, data);
  }

}
