import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, PopoverController } from '@ionic/angular';
import { UserService } from 'src/app/user/service/user-service.service';

@Component({
  selector: 'app-parent-menu',
  templateUrl: './parent-menu.component.html',
  styleUrls: ['./parent-menu.component.scss'],
})
export class ParentMenuComponent implements OnInit {
  course: any;
  constructor(
    public popoverController: PopoverController,
    private userservice: UserService,
    private navParams: NavParams,
    private router: Router
  ) { 
    this.course = this.navParams.get('course');
  }

  ngOnInit() { }

  closeCourseMenu() {
    this.popoverController.dismiss();
  }


  // Log out from the system
  logout() {
    this.closeCourseMenu();
    this.userservice.logout();
    this.router.navigate(['home']);
  }

  profile() {
    this.closeCourseMenu();
    this.router.navigate(['user/user-profil']);
  }

}
