import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParentDashPage } from './parent-dash.page';

const routes: Routes = [
    {
        path: '',
        component: ParentDashPage
    },
    { path: 'classes', loadChildren: () => import('./classes/classes.module').then(m => m.ClassesPageModule) }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ParentDashRoutingModule {

}
