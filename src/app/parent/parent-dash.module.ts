import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { ParentDashRoutingModule } from './parent-dash-routing.module';
import { ParentDashPage } from './parent-dash.page';
import { ParentMenuComponent } from './parent-menu/parent-menu.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    ParentDashRoutingModule
  ],
  declarations: [
    ParentDashPage,
    ParentMenuComponent
  ],
  entryComponents: [
    ParentMenuComponent
  ]
})
export class ParentDashPageModule {}
