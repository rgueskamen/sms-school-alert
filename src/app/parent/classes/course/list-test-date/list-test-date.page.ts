import { Component, OnInit } from '@angular/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { ActivatedRoute } from '@angular/router';
import { ProgrammeService } from '../../../service/programme-service.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { ErrorProgrammeService } from '../../../service/error-programme.service';

@Component({
  selector: 'app-list-test-date',
  templateUrl: './list-test-date.page.html',
  styleUrls: ['./list-test-date.page.scss'],
})
export class ListTestDatePage implements OnInit {

  classes: any;
  loading: boolean;
  classId: any;
  courseId: any;
  programs: any;
  courseData: any;
  selectedProgram: any;
  activeProgram: any;

  constructor(
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private error: ErrorSystemService,
    private activated: ActivatedRoute,
    private utils: UtilsService
  ) {
    this.programs = [];
    this.selectedProgram = [];
    this.loading = true;
    this.classId = this.activated.snapshot.params.classId;
    this.courseId = this.activated.snapshot.params.courseId;
  }

  ngOnInit() {
    this.getProgramDateList(null);
  }

  doRefresh(event) {
    this.getProgramDateList(event);
  }

  // format the course reponse data
  formatProgramResponseData(data: any) {
    if (data && data.message === "success") {
      this.programs = data.date_depot;
      this.programs =  this.utils.orderByKeyDown(this.programs,'active');
      this.courseData =  data.infos_matiere;
    }
  }

  // Get the program list
  getProgramDateList(event: any) {
    this.program.getProgrammTestDate(this.classId,this.courseId).subscribe((reponse: any) => {
      this.formatProgramResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getProgramDateList(event);
            } else {
              this.loading = false;
            }
          });
        } else {
          this.loading = false;
          this.programError.managePlanningError(error);
        }
      } else {
        this.loading = false;
        this.error.manageSystemError(error);
      }
    });
  }

 
}
