import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { SharedModule } from 'src/app/shared/shared.module';
import { ListTestDatePage } from './list-test-date.page';
import { ListTestDatePageRoutingModule } from './list-test-date-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    ListTestDatePageRoutingModule
  ],
  declarations: [ListTestDatePage]
})
export class ListTestDatePageModule {}
