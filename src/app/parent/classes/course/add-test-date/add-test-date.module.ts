import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddTestDatePage } from './add-test-date.page';
import { AddTestDatePageRoutingModule } from './add-test-date.routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    AddTestDatePageRoutingModule
  ],
  declarations: [AddTestDatePage]
})
export class AddTestDateDocPageModule {}
