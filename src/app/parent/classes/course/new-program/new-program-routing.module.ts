import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewProgramPage } from './new-program.page';

const routes: Routes = [
  {
    path: '',
    component: NewProgramPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewProgramPageRoutingModule {}
