import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsPageRoutingModule } from './course-routing.module';

import { SharedModule } from '../../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { CoursePage } from './course.page';
import { CourseMenuComponent } from './course-menu/course-menu.component';
import { ClassMenuComponent } from './class-menu/class-menu.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    NewsPageRoutingModule,
    SharedModule
  ],
  declarations: [
    CoursePage,
    CourseMenuComponent,
    ClassMenuComponent
  ],
  entryComponents: [
    CourseMenuComponent,
    ClassMenuComponent
  ]
})
export class CoursePageModule { }
