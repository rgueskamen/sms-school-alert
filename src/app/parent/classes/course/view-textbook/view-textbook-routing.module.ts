import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewTextBookPage } from './view-textbook.page';

const routes: Routes = [
  {
    path: '',
    component: ViewTextBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ViewTeextBookPageRoutingModule { }
