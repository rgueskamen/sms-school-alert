import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { UserService } from 'src/app/user/service/user-service.service';
import { CoursService } from '../../../service/cours.service';
import { ErrorCoursService } from '../../../service/error-cours.service';
import { TextBookMenuComponent } from './textbook-menu/textbook-menu.component';

@Component({
  selector: 'app-view-textbook',
  templateUrl: 'view-textbook.page.html',
  styleUrls: ['view-textbook.page.scss']
})
export class ViewTextBookPage {
  textbooks: any[] = [];
  loading: boolean;
  courseID: any;
  classID: any;
  currentUser: any;
  teacherData:any;
  courseData: any;
  classData: any;

  constructor(
    private userService: UserService,
    private course: CoursService,
    private activeRoute: ActivatedRoute,
    private event: EventsService,
    private courseError: ErrorCoursService,
    private error: ErrorSystemService,
    private popoverController: PopoverController
  ) {
    this.loading = true;
    this.courseID = this.activeRoute.snapshot.params.courseId;
    this.classID = this.activeRoute.snapshot.params.classId;
    this.currentUser = this.userService.getUserData();
    this.event.subscribe('update-text-book', () => {
      this.loading = true;
      this.getTextBookList(null);
    });
  }

  ngOnInit(): void {
    this.getTextBookList(null);
  }

  doRefresh(event) {
    this.getTextBookList(event);
  }

  // open the textbook menu
  async openMenu(ev, data: any) {
    const popover = await this.popoverController.create({
      component: TextBookMenuComponent,
      event: ev,
      componentProps: {
        textbook: data,
        courseId: this.courseID,
        classId: this.classID
      }
    });
    return await popover.present();
  }

  // format the textbook reponse data
  formatTextBookResponseData(data: any) {
    console.log(data);
    if (data && data.message === "success") {
      this.textbooks = data.cahier_texte;
      this.teacherData = data.infos_prof;
      this.courseData = data.infos_matiere;
      this.classData = data.infos_classe;
    }
  }

  // Get the textBook list
  getTextBookList(event: any) {
    const currentUser = this.userService.getUserData();
    this.course.getClassTextBookInfo(currentUser.etablissement_scolaire_id, this.classID, this.courseID).subscribe((reponse: any) => {
      this.formatTextBookResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {

      this.loading = false;
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.loading = true;
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getTextBookList(event);
            } else {
              this.loading = false;
            }
          });

        } else {
          this.courseError.manageCoursesError(error);
        }

      } else {
        this.error.manageSystemError(error);
      }

    });
  }

}
