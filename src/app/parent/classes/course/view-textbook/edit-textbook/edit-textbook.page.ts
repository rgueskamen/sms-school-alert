import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, NavParams } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { UserService } from 'src/app/user/service/user-service.service';
import { CoursService } from '../../../../service/cours.service';
import { ErrorCoursService } from '../../../../service/error-cours.service';
import { ErrorSyllabusService } from '../../../../service/error-syllabus.service';
import { SyllabusService } from '../../../../service/syllabus.service';

@Component({
  selector: 'app-edit-textbook',
  templateUrl: './edit-textbook.page.html',
  styleUrls: ['./edit-textbook.page.scss'],
})
export class EditTextBookPage implements OnInit {
  loading: boolean;
  textbookForm: FormGroup;
  validationMessages: any;
  courseDataId: any;
  classDataId: any;
  chapters: any[];
  subChapters: any[];
  subTitles: any[];
  currentUser: any;
  currentTextbookData: any;

  constructor(
    private fb: FormBuilder,
    private location: TranslateService,
    private course: CoursService,
    private courseError: ErrorCoursService,
    private navParams: NavParams,
    private userService: UserService,
    private errorService: ErrorSystemService,
    private event: EventsService,
    private utils: UtilsService,
    private syllabus: SyllabusService,
    private syllabusError: ErrorSyllabusService,
    private error: ErrorSystemService,
    private modatCtrl: ModalController,
    private router: Router
  ) {
    this.chapters = [];
    this.subChapters = [];
    this.subTitles = [];
    this.loading = false;
    this.currentUser =  this.userService.getUserData();
    this.currentTextbookData = this.navParams.get('textbook');

    this.courseDataId = this.navParams.get('courseID');
    this.classDataId = this.navParams.get('classID');
  }

  ngOnInit() {
    this.validationMessage();
    this.initUserForm();
  }

  // Form getters 
  get schoolId() {
    return this.textbookForm.get('etablissement_scolaire_id');
  }

  get classId() {
    return this.textbookForm.get('classe_id');
  }

  get courseId() {
    return this.textbookForm.get('matiere_id');
  }

  get teacherId() {
    return this.textbookForm.get('professeur_id');
  }

  get chapterId() {
    return this.textbookForm.get('chapitre_id');
  }

  get subChapterId() {
    return this.textbookForm.get('sous_chapitre_id');
  }

  get subTitleId() {
    return this.textbookForm.get('sous_titre_chapitre_id');
  }

  get advise() {
    return this.textbookForm.get('avis_prof');
  }

  // init the user form with his data
  initUserForm() {
    const user = this.userService.getUserData();
    this.textbookForm = this.fb.group({
      cahier_texte_id:[this.currentTextbookData.cahier_texte_id, Validators.required],
      etablissement_scolaire_id: [parseInt(user.etablissement_scolaire_id), Validators.required],
      classe_id: [parseInt(this.classDataId), Validators.required],
      matiere_id: [parseInt(this.courseDataId), Validators.required],
      professeur_id: [user.id],
      chapitre_id: [this.currentTextbookData.infos_chapitre.id, Validators.required],
      sous_chapitre_id: [this.currentTextbookData.infos_sous_chapitre.id, Validators.required],
      sous_titre_chapitre_id: [this.currentTextbookData.infos_sous_titre_sous_chapitre.id, Validators.required],
      avis_prof: [this.currentTextbookData.avis_prof, Validators.required]
    });
    this.getAllChapter(this.classDataId,this.courseDataId);
    this.getAllSubChapter(this.currentTextbookData.infos_chapitre.id, this.classDataId,this.courseDataId);
    this.getAllSubChapterSubTitle(this.currentTextbookData.infos_sous_chapitre.id, this.classDataId,this.courseDataId); 
  }

  // Validation messagvalidationMessagese
  validationMessage() {
    this.location.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        schoolId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        classId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        courseId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        teacherId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        chapterId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        subChapterId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        subTitleId: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
        , advise: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]
      };
    });
  }

  // close the modal
  closeModal() {
    this.modatCtrl.dismiss();
  }

  // get the list of chapter
  getAllChapter(classId: any, courseId: any) {
    this.syllabus.getChapter(classId, courseId)
      .subscribe((reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.chapters = this.utils.orderByKeyUp(reponse.chapitres, 'numero');
        }
      }, error => {
        if (error && error.error && error.error.message === "error") {
          if (error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === "OK") {
                this.getAllChapter(classId, courseId);
              }
            });
          } else {
            this.syllabusError.manageSyllabusError(error);
          }
        } else {
          this.error.manageSystemError(error);
        }
      });
  }

  // get the list of subchapter
  getAllSubChapter(chapterId: any, classId: any, courseId: any) {
    this.syllabus.getSubChapter(chapterId, classId, courseId)
      .subscribe((reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.subChapters = this.utils.orderByKeyUp(reponse.sous_chapitres, 'numero');
        }
      }, error => {
        if (error && error.error && error.error.message === "error") {
          if (error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === "OK") {
                this.getAllSubChapter(chapterId, classId, courseId);
              }
            });
          } else {
            this.syllabusError.manageSyllabusError(error);
          }
        } else {
          this.error.manageSystemError(error);
        }
      });
  }

  // get the list of subchapter
  getAllSubChapterSubTitle(subChapterId: any, classId: any, courseId: any) {
    this.syllabus.getSubTitleSubChapter(subChapterId, classId, courseId)
      .subscribe((reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.subTitles = this.utils.orderByKeyUp(reponse.sous_titre_chapitres, 'numero');
        }
      }, error => {
        if (error && error.error && error.error.message === "error") {
          if (error.error.user_not_found) {
            this.error.renewSession().then((data: any) => {
              if (data && data.result === "OK") {
                this.getAllSubChapter(subChapterId, classId, courseId);
              }
            });
          } else {
            this.syllabusError.manageSyllabusError(error);
          }
        } else {
          this.error.manageSystemError(error);
        }
      });
  }

  // update the textbook fullfill
  updateTextbookFullfill() {
    this.loading = true;
    this.course.updateTestBookInfo(this.textbookForm.value).subscribe(
      (reponse: any) => {
        if (reponse && reponse.message === "success") {
          this.event.publish('update-text-book');
          this.router.navigate(['dash/textbook/view',this.classDataId , this.courseDataId]);
         
        }
        this.loading = false;
        this.closeModal();
      }, error => {
        
        if (error && error.error) {
          if (error && error.error && error.error.user_not_found) {
            this.errorService.renewSession().then((data: any) => {
              if (data && data.result === "OK") {
                this.updateTextbookFullfill();
              } else {
                this.loading = false;
                this.closeModal();
              }
            });
          } else {
            this.loading = false;
            this.closeModal();
            this.courseError.manageCoursesError(error);
          }
        } else {
          this.loading = false;
          this.closeModal();
          this.errorService.manageSystemError(error);
        }
      });
  }

}
