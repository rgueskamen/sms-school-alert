import { Component } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { UtilsService } from 'src/app/shared/service/utils.service';
import { CoursService } from '../../../../service/cours.service';
import { ErrorCoursService } from '../../../../service/error-cours.service';
import { EtabliServiceService } from 'src/app/school/service/admin-school-service.service';

@Component({
  selector: 'app-student-advise',
  templateUrl: 'student-advise.page.html',
  styleUrls: ['student-advise.page.scss']
})
export class StudentAdvisePage {

  studentAdvise: any;
  textBook: any;
  loading: boolean;
  classId: any;
  studentList: any;
  studentSchool: any;

  constructor(
    private course: CoursService,
    private courseError: ErrorCoursService,
    private admin: EtabliServiceService,
    private navparam: NavParams,
    private modatCtrl: ModalController,
    private util: UtilsService,
    private error: ErrorSystemService
  ) {
    this.studentAdvise = [];
    this.loading = true;
    this.textBook = this.navparam.get('textbook');
    this.classId = this.navparam.get('classId');
    this.studentList = [];
  }


  ngOnInit(): void {
    this.getStudentAdviseList(null);
    this.getStudentList();
  }

  doRefresh(event) {
    this.getStudentAdviseList(event);
  }

    // close the modal
    closeModal() {
      this.modatCtrl.dismiss();
    }


  // format student qdvise reponse data
  formatStudentAdviseResponseData(data: any) {
    if (data && data.message === "success") {
      this.studentAdvise = this.util.orderByKeyUp(data.avis_eleve, 'updated_at');
    }
  }

  // Get the student advise
  getStudentAdviseList(event: any) {

    this.course.getStudentAdvise(this.textBook.cahier_texte_id).subscribe((reponse: any) => {
      this.formatStudentAdviseResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {

      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getStudentAdviseList(event);
            } else {
              this.loading = false;
            }
          });

        } else {
          this.loading = false;
          this.courseError.manageCoursesError(error);
        }

      } else {
        this.loading = false;
        this.error.manageSystemError(error);
      }

    });
  }

  // Get the list of student
  getStudentList() {
    this.admin.getStudentWithParent(this.classId).subscribe((reponse: any) => {
      console.log(reponse);
      if (reponse && reponse.message === "success") {
        this.studentList = reponse.eleves;
        this.studentSchool = reponse.etablissement_scolaire;
      }
    }, error => {

      if (error && error.error && error.error.message === 'error') {

        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getStudentList();
            }
          });

        } else {
          this.courseError.manageCoursesError(error);
        }

      } else {
        this.error.manageSystemError(error);
      }

    });
  }


  // get the  current student data
  getCurrentStudentData(studentList: any[], studentId: number) {
    if (studentList && studentList.length > 0) {
     return studentList.find((value,index,student) => { return student[index].eleve.id === studentId});
    } else return [];
  }

}
