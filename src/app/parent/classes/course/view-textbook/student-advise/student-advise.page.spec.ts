import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAdvisePage } from './student-advise.page';

describe('ExplorePage', () => {
  let component: StudentAdvisePage;
  let fixture: ComponentFixture<StudentAdvisePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(StudentAdvisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
