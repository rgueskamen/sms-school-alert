import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../../shared/shared.module';
import { IonicModule } from '@ionic/angular';
import { ViewTextBookPage } from './view-textbook.page';
import { ViewTeextBookPageRoutingModule } from './view-textbook-routing.module';
import { TextBookMenuComponent } from './textbook-menu/textbook-menu.component';
import { EditTextBookPage } from './edit-textbook/edit-textbook.page';
import { DeleteTextBookComponent } from './delete-textbook/delete-textbook.component';
import { StudentAdvisePage } from './student-advise/student-advise.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    ViewTeextBookPageRoutingModule,
    SharedModule
  ],
  declarations: [
    ViewTextBookPage,
    TextBookMenuComponent,
    DeleteTextBookComponent,
    StudentAdvisePage,
    EditTextBookPage
  ],
  entryComponents: [
    TextBookMenuComponent,
    DeleteTextBookComponent,
    StudentAdvisePage,
    EditTextBookPage
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ViewTextBookPageModule { }
