import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FufillTextBookPage } from './fufill-textbook.page';



const routes: Routes = [
  {
    path: '',
    component: FufillTextBookPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FuffillTextBookPageRoutingModule {}
