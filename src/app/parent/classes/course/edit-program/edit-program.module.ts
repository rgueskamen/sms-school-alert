import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditProgramPage } from './edit-program.page';
import { NewProgramPageRoutingModule } from './edit-program-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    IonicModule,
    NewProgramPageRoutingModule
  ],
  declarations: [EditProgramPage]
})
export class EditProgramPageModule {}
