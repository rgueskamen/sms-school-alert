import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ErrorSystemService } from 'src/app/shared/service/error-system.service';
import { EventsService } from 'src/app/shared/service/events.service';
import { PluginService } from 'src/app/shared/service/plugin.service';
import { UiService } from 'src/app/shared/service/ui.service';
import { ErrorProgrammeService } from '../../../service/error-programme.service';
import { ProgrammeService } from '../../../service/programme-service.service';

@Component({
  selector: 'app-edit-program',
  templateUrl: './edit-program.page.html',
  styleUrls: ['./edit-program.page.scss'],
})
export class EditProgramPage implements OnInit {

  dataForm: FormGroup;
  validationMessages: any;
  loading: boolean;
  classId: any;
  programs:any;
  @ViewChild('fileInput',{static: false}) fileInputClick: ElementRef;

  constructor(
    private fb: FormBuilder,
    private plugin: PluginService,
    private event: EventsService,
    private program: ProgrammeService,
    private programError: ErrorProgrammeService,
    private activated: ActivatedRoute,
    private ui: UiService,
    private translate: TranslateService,
    private error: ErrorSystemService,
    private zone: NgZone
  ) {
      this.loading = false;
      this.programs = [];
      this.classId = this.activated.snapshot.params.classId;
   }

  ngOnInit() {
    this.initFormData();
    this.getProgramList(null);
    this.validationMessage();
  }

  doRefresh(event) {
    this.getProgramList(event);
  }

  buttonClick() {
    this.fileInputClick.nativeElement.click();    
  }

  // Form validation
  validationMessage() {
    this.translate.get(['FORM_FIELD_REQUIRED_ERROR_TEXT']).subscribe(trans => {
      this.validationMessages = {
        programme_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        classe_id: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ],
        programme: [
          { type: 'required', message: trans.FORM_FIELD_REQUIRED_ERROR_TEXT }
        ]

      };
    });
  }

  // Form getters
  get programme_id() {
    return this.dataForm.get('programme_id');
  }

  get classe_id() {
    return this.dataForm.get('classe_id');
  }

  get programme() {
    return this.dataForm.get('programme');
  }

  // Init the form 
  initFormData() {
    this.dataForm = this.fb.group({
      programme_id:['',Validators.required],
      classe_id: [this.classId, Validators.required],
      programme: ['', Validators.required]
    });
  }

  // Get the documents file
    getFile(event) {
      this.plugin.getFile(event).subscribe((file: any) => {
        if (file) {
          this.zone.run(() => {
            this.dataForm.get('programme').setValue(file);
          });
        }
      });
    }

  
  // new programm
  editProgram() {
    this.loading = true;
    let param = this.dataForm.value;
    this.program.updateTestPlanning(param).subscribe(
      (reponse: any) => {
        this.loading = false;
        if (reponse && reponse.message === 'success') {
          this.initFormData();
          this.translate.get('PRODRAM_UPDATE_MSG').subscribe(trans => {
            this.ui.presentToast(trans);
          });
          this.event.publish('new-program');
        }
      }, error => {
     
        if (error && error.error && error.error.message === "error") {

          if (error.error.user_not_found) {
            this.error.renewSession().then((data:any) => {
                  if (data && data.result === "OK") {
                      this.editProgram();
                  } else {
                    this.loading = false;
                  }
            });
          } else {
            this.loading = false;
            this.programError.managePlanningError(error);
          }
       
        } else {
          this.loading = false;
          this.error.manageSystemError(error);
        }
    });
  }

    // show document
    showDoc() {
      if (this.dataForm.value.programme) {
        this.plugin.showFile(this.dataForm.value.programme);
      } else {
        this.translate.get('NONE_DOCUMENT').subscribe(trans => {
          this.ui.presentToast(trans);
        });
      }
  }

    // format the course reponse data
    formatProgramResponseData(data: any) {
      if (data && data.message === "success") {
        this.programs = data.programme;
        this.zone.run(() => {
          this.programs = this.programs.filter(data => {
            return data.active === 0;
          });
        });
     
        if (this.programs && this.programs.length > 0) {
            setTimeout(() => {
              this.dataForm.get('programme_id').setValue(this.programs[0].id);
            }, 200);
        }
      }
    }

   // Get the program list
   getProgramList(event: any) {
    this.program.getClassProgram(this.classId).subscribe((reponse: any) => {
      this.formatProgramResponseData(reponse);
      this.loading = false;
      if (event) {
        setTimeout(() => {
          event.target.complete();
        }, 200);
      }
    }, error => {
      if (event) {
        event.target.complete();
      }
      if (error && error.error && error.error.message === 'error') {
        if (error && error.error && error.error.user_not_found) {
          this.error.renewSession().then((data: any) => {
            if (data && data.result === "OK") {
              this.getProgramList(event);
            } else {
              this.loading = false;
            }
          });
        } else {
          this.loading = false;
          this.programError.managePlanningError(error);
        }
      } else {
        this.loading = false;
        this.error.manageSystemError(error);
      }
    });
  }

}
