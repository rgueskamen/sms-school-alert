import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-course-menu',
  templateUrl: './course-menu.component.html',
  styleUrls: ['./course-menu.component.scss'],
})
export class CourseMenuComponent implements OnInit {
  course: any;
  classeId: any;
  constructor(
    public popoverController: PopoverController,
    private navParams: NavParams,
    private router: Router
  ) { 
    this.course = this.navParams.get('course');
    this.classeId =  this.navParams.get('classId');
  }

  ngOnInit() { }

  closeCourseMenu() {
    this.popoverController.dismiss();
  }

  // Fullfil the text book
  fillTextBook() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','textbook',this.course.id]);
  }

  // View textbook
  viewTextBook() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','textbook','view',this.course.id]);
  }

  // Add program test date
  addTestDate() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','new-test-date',this.course.id]);
   }

  // Get date exam
  getTestDate() {
    this.closeCourseMenu();
    this.router.navigate(['teacher/classes',this.classeId,'courses','list-test-date',this.course.id]);
  }

}
